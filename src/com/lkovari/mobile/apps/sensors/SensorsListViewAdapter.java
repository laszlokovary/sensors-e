package com.lkovari.mobile.apps.sensors;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author lkovari
 *
 */
public class SensorsListViewAdapter extends BaseAdapter {
	private static ArrayList<SensorDescriptor> sensorDescriptorArrayList;
	private Integer[] imgid = {R.drawable.available_32x32, R.drawable.notavailable_32x32};

	private LayoutInflater mInflater;
	
	public SensorsListViewAdapter(Context context, ArrayList<SensorDescriptor> results) {
		sensorDescriptorArrayList = results;
		mInflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return this.sensorDescriptorArrayList.size();
	}

	@Override
	public Object getItem(int ix) {
		return this.sensorDescriptorArrayList.get(ix);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_view_row, null);
			holder = new ViewHolder();
			holder.imgStatus = (ImageView) convertView.findViewById(R.id.status);
			holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
			holder.txtName = (TextView) convertView.findViewById(R.id.name);
			holder.txtDetails = (TextView) convertView.findViewById(R.id.details);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.imgStatus.setImageResource(imgid[sensorDescriptorArrayList.get(pos).getImageNumber()]);
		holder.txtTitle.setText(sensorDescriptorArrayList.get(pos).getTitle());
		holder.txtName.setText(sensorDescriptorArrayList.get(pos).getName());
		holder.txtDetails.setText(sensorDescriptorArrayList.get(pos).getDetails());
		holder.type = sensorDescriptorArrayList.get(pos).getType();
		return convertView;
	}

	static class ViewHolder {
		ImageView imgStatus;
		TextView txtTitle;
		TextView txtName;
		TextView txtDetails;
		int type;
	}	
}
