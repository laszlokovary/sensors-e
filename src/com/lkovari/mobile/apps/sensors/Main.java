package com.lkovari.mobile.apps.sensors;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * http://developer.android.com/reference/android/hardware/Sensor.html
 */
 public class Main extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback {
	public static String BUNDLE_CONTENT_TAG_DATA1 = "D1";	
	public static String BUNDLE_CONTENT_TAG_DATA2 = "D2";	
	public static String BUNDLE_CONTENT_TAG_DATA3 = "D2";
	
	private static final int REQUEST_WRITE_STORAGE = 112;
	
	private StringBuffer sb = null;
	
	private SensorManager sensorManager = null;
	private Context context;
    private Activity sensorActivity;

    private int[] sensorTypes = new int[] {
			//#3
			Sensor.TYPE_ACCELEROMETER,
			//#3
			Sensor.TYPE_GYROSCOPE,
			//#3
			Sensor.TYPE_LIGHT,
    		//#3
    		Sensor.TYPE_MAGNETIC_FIELD,
    		//#3
    		Sensor.TYPE_ORIENTATION,
    		//#3
    		Sensor.TYPE_PRESSURE,
    		//#3
    		Sensor.TYPE_TEMPERATURE,
    		//#3
    		Sensor.TYPE_PROXIMITY,
    		//#9
    		Sensor.TYPE_GRAVITY,
    		//#9
    		Sensor.TYPE_LINEAR_ACCELERATION,
    		//#9
    		Sensor.TYPE_ROTATION_VECTOR,
    		//#14
    		Sensor.TYPE_RELATIVE_HUMIDITY,
    		//#14
    		Sensor.TYPE_AMBIENT_TEMPERATURE,
    		//#18
    		Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED,
    		//#18
    		Sensor.TYPE_GAME_ROTATION_VECTOR,
    		//#18
    		Sensor.TYPE_GYROSCOPE_UNCALIBRATED,
    		//#18
    		Sensor.TYPE_SIGNIFICANT_MOTION,
    		//#19
    		Sensor.TYPE_STEP_DETECTOR,
    		//#19
    		Sensor.TYPE_STEP_COUNTER,
    		//#19
    		Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR,
    		//#20
    		Sensor.TYPE_HEART_RATE,
    		/*
    	    Sensor.TYPE_WAKE_GESTURE,
    	    Sensor.TYPE_GLANCE_GESTURE,
			Sensor.TYPE_PICK_UP_GESTURE,
			Sensor.TYPE_WRIST_TILT_GESTURE,
			Sensor.TYPE_DEVICE_ORIENTATION,
			*/
    		
    		SensorUtils.TYPE_TILT_DETECTOR,
    		SensorUtils.TYPE_PICK_UP_GESTURE,
    		
    		//#24
			Sensor.TYPE_POSE_6DOF,
			Sensor.TYPE_STATIONARY_DETECT,
			Sensor.TYPE_MOTION_DETECT,
			Sensor.TYPE_HEART_BEAT,
			
			//Sensor.TYPE_DYNAMIC_SENSOR_META,    		
    		
			// Private based "unknown" sensors
			SensorUtils.TYPE_SCREEN_ORIENTATION_V3,
			SensorUtils.TYPE_GRIP_SENSOR_V512,
			SensorUtils.TYPE_HRMLED_IR_SENSOR_V1,
			SensorUtils.TYPE_HRMLED_RED_SENSOR_V1,
			SensorUtils.TYPE_RGB_IR_SENSOR_V1,
			SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1,
			
		  };
    
    

    
    TextView authorNameTextView = null;
    TextView authorTextView = null;
    
    ListView sensorListView = null;
    
    private SensorListener sensorEventListener = new SensorListener();

//	Sensor sensor = null;
    
	private ArrayList<SensorDescriptor> sensorDescriptorList = new ArrayList<SensorDescriptor>();
	
	private int numberOfAllSensors = 0;
	private int numberOfAvailableSensors = 0;
	
	private int selectedType = -1;

	private SensorsShowOptionKind sensorsShowOptionKind = SensorSettings.SENSOR_SHOW_OPTION_KIND;
	
	private TextView deviceNameTextView = null;
	
	private TriggerListener triggerListener = null;
	private String availableTitle = null;
	
	@SuppressLint("NewApi")
	class TriggerListener extends TriggerEventListener {
		@Override
		public void onTrigger(TriggerEvent event) {
			
		}
	}	
	
	class SensorUncaughtExceptionHandler implements UncaughtExceptionHandler {
		private UncaughtExceptionHandler defaultUEH;
		private Exception throwable = null;
		private Context context;
		
		
		public SensorUncaughtExceptionHandler(Context context) {
			this.context = context;
	        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		}
		
		
		public Context getContext() {
			return context;
		}

		
	    public void uncaughtException(Thread t, Throwable ex) {
	        final Writer result = new StringWriter();
	        final PrintWriter printWriter = new PrintWriter(result);
	        throwable.printStackTrace(printWriter);
	        String stacktrace = result.toString();
	        printWriter.close();
	        Toast.makeText(context, stacktrace, Toast.LENGTH_LONG).show();
        	SDCardManager.createErrorLog(context, throwable);
	        defaultUEH.uncaughtException(t, ex);
	    }

	
	}
	
	
	private void passCommonInfo(Activity currentContent, Intent moduleIntent, Long data1, Long data2, Long data3) {
		Bundle b = new Bundle();
		b.putLong(BUNDLE_CONTENT_TAG_DATA1, data1);
		b.putLong(BUNDLE_CONTENT_TAG_DATA2, data2);
		b.putLong(BUNDLE_CONTENT_TAG_DATA3, data3);
		moduleIntent.putExtras(b);
	}

	private String extractSensorUnregMess(int type) {
		String sensorUnregMess = "";
		if (type == Sensor.TYPE_ACCELEROMETER) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_accelerometer_sensor_listener);
		}
		else if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_ambienttemperature_sensor_listener);
		}
		else if (type == Sensor.TYPE_GRAVITY) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_gravity_sensor_listener);
		}
		else if (type == Sensor.TYPE_GYROSCOPE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_gyroscope_sensor_listener);
		}
		else if (type == Sensor.TYPE_LIGHT) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_light_sensor_listener);
		}
		else if (type == Sensor.TYPE_LINEAR_ACCELERATION) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_linearacceleration_sensor_listener);
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_magneticfield_sensor_listener);
		}
		else if (type == Sensor.TYPE_ORIENTATION) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_orientation_sensor_listener);
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_pressure_sensor_listener);
		}
		else if (type == Sensor.TYPE_PROXIMITY) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_proximity_sensor_listener);
		}
		else if (type == Sensor.TYPE_RELATIVE_HUMIDITY) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_relativehumidity_sensor_listener);
		}
		else if (type == Sensor.TYPE_ROTATION_VECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_rotationvector_sensor_listener);
		}
		else if (type == Sensor.TYPE_TEMPERATURE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_temperature_sensor_listener);
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_magneticfieldu_sensor_listener);
		}
		else if (type == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_geomagrotationvector_sensor_listener);
		}
		else if (type == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_gamerotationvector_sensor_listener);
		}
		else if (type == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_gyroscopeu_sensor_listener);
		}
		else if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_significantmotion_sensor_listener);
		}
		else if (type == Sensor.TYPE_STEP_COUNTER) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_stepcounter_sensor_listener);
		}
		else if (type == Sensor.TYPE_STEP_DETECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_stepdetector_sensor_listener);
		}
		else if (type == Sensor.TYPE_HEART_RATE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_heartrate_sensor_listener);
		}
		else if (type == SensorUtils.TYPE_TILT_DETECTOR) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_tilt_detector_sensor_listener);
		}
		else if (type == SensorUtils.TYPE_PICK_UP_GESTURE) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_pickup_gesture_sensor_listener);
		}
		else if (type == Sensor.TYPE_STATIONARY_DETECT) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_stationarydetect_sensor_listener);
		}
		else if (type == Sensor.TYPE_MOTION_DETECT) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_motiondetect_sensor_listener);
		}
		else if (type == Sensor.TYPE_HEART_BEAT) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_hearthbeat_sensor_listener);
		}
		else if (type == Sensor.TYPE_POSE_6DOF) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_posedof_sensor_listener);
		}
		else if (type == SensorUtils.TYPE_SCREEN_ORIENTATION_V3) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_screen_orientation_sensor_listener);
		}
		else if (type == SensorUtils.TYPE_GRIP_SENSOR_V512) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_grip_sensor_listener);
		}
		else if (type == SensorUtils.TYPE_HRMLED_IR_SENSOR_V1) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_hrmled_ir_sensor_listener);
		}
		else if (type == SensorUtils.TYPE_HRMLED_RED_SENSOR_V1) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_hrmled_red_sensor_listener);
		}
		else if (type == SensorUtils.TYPE_RGB_IR_SENSOR_V1) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_rgb_ir_sensor_listener);
		}
		else if (type == SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1) {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_interrupt_gyroscope_sensor_listener);
		}
		else {
			sensorUnregMess = getResources().getString(R.string.doesnot_registered_sensor_listener);
		}
		return sensorUnregMess;
	}
	
	private boolean isSensorStandard(int type) {
		boolean isSensorTypeExists = false;
		for (int sensorType : sensorTypes) {
			if (sensorType == type) {
				isSensorTypeExists = true;
				break;
			}
		}
		return isSensorTypeExists;
	}
	
	
	/**
	 *
	 * @param type int - Sensor type
	 * @return boolean - true if exists in the current version else false
	 */
	public boolean isSensorExistsInSDKVersion(int type) {
		boolean isExistsInVersion = false;
		int sdkVersion = android.os.Build.VERSION.SDK_INT;
		switch (type) {
			case Sensor.TYPE_ACCELEROMETER :
			case Sensor.TYPE_GYROSCOPE :
			case Sensor.TYPE_LIGHT :
			case Sensor.TYPE_MAGNETIC_FIELD :
			case Sensor.TYPE_ORIENTATION :
			case Sensor.TYPE_PRESSURE :
			case Sensor.TYPE_PROXIMITY :
			case Sensor.TYPE_TEMPERATURE : {
				isExistsInVersion = (sdkVersion >= 8);
				break;
			}
			case Sensor.TYPE_GRAVITY :
			case Sensor.TYPE_LINEAR_ACCELERATION :
			case Sensor.TYPE_ROTATION_VECTOR : {
				isExistsInVersion = (sdkVersion >= 9);
				break;
			}

			case Sensor.TYPE_RELATIVE_HUMIDITY :
			case Sensor.TYPE_AMBIENT_TEMPERATURE : {
				isExistsInVersion = (sdkVersion >= 14);
				break;
			}

			case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED :
			case Sensor.TYPE_GAME_ROTATION_VECTOR :
			case Sensor.TYPE_GYROSCOPE_UNCALIBRATED :
			case Sensor.TYPE_SIGNIFICANT_MOTION : {
					isExistsInVersion = (sdkVersion >= 18);
				break;
			}

			case Sensor.TYPE_STEP_DETECTOR :
			case Sensor.TYPE_STEP_COUNTER :
			case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR : {
				isExistsInVersion = (sdkVersion >= 19);
				break;
			}

			case Sensor.TYPE_HEART_RATE : {
				isExistsInVersion = (sdkVersion >= 20);
				break;
			}
			
			case SensorUtils.TYPE_TILT_DETECTOR:
			case SensorUtils.TYPE_PICK_UP_GESTURE:
			case SensorUtils.TYPE_SCREEN_ORIENTATION_V3:
			case SensorUtils.TYPE_GRIP_SENSOR_V512:
			case SensorUtils.TYPE_HRMLED_IR_SENSOR_V1:
			case SensorUtils.TYPE_HRMLED_RED_SENSOR_V1:
			case SensorUtils.TYPE_RGB_IR_SENSOR_V1:
			case SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1: {
				isExistsInVersion = (sdkVersion >= 22);
				break;
			}
			
			//case Sensor.TYPE_DEVICE_PRIVATE_BASE :
			case Sensor.TYPE_STATIONARY_DETECT :
			case Sensor.TYPE_MOTION_DETECT :
			case Sensor.TYPE_HEART_BEAT :
			case Sensor.TYPE_POSE_6DOF : {
				isExistsInVersion = (sdkVersion >= 24);
				break;
			}
			default : {
				isExistsInVersion = true;
			}
		}
		return isExistsInVersion;
	}	
	
	/**
	 * 
	 * @param sensorList List<Sensor> - list of available sensors
	 * @param type int - type of sensor
	 * @return boolean
	 */
	private boolean isFoundSensorTypeInSensorList(List<Sensor> sensorList, Integer type) {
		boolean isFound = false;
		for (Sensor s : sensorList) {
			Integer t1 = s.getType();
			Integer t2 = type;
			if (t1.equals(t2)) {
				isFound = true;
				break;
			}
		}
		return isFound;
	}
	
	/**
	 * 
	 * @param sd SensorDescriptor - describe a sensor
	 * @param imageNumber int - if 0 then exists if 1 then not exists
	 * @param title String - title of sensor
	 * @param name String - name of sensor
	 * @param details String - details of sensor
	 * @param type int - type sensor type
	 * @param isAvailable boolean - if true then available else not available
	 */
	private void setupDescriptor(SensorDescriptor sd, int imageNumber, String title, String name, String details, Integer type, boolean isAvailable) {
		if (sd != null) {
			sd.setImageNumber(imageNumber);
			sd.setTitle(title);
			sd.setName(name);
			sd.setDetails(details);
			sd.setType(type);
			sd.setAvailable(isAvailable);
		}
	}
	

	/**
	 * 
	 * @param activity
	 */
	private void checkEsternalStoragePermission(Activity activity) {
    	if (Build.VERSION.SDK_INT >= 23) {
    		boolean hasPermission = (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    		if (!hasPermission) {
    			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
    		}		
    	}
	}	
	
    @TargetApi(23) @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    	if (Build.VERSION.SDK_INT >= 23) {
    		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    		switch (requestCode) {
    		case REQUEST_WRITE_STORAGE: {
    			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
    				Toast.makeText(Main.this, "The app was allowed to write to your storage.", Toast.LENGTH_LONG).show();
    			} else {
    				Toast.makeText(this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
    			}
    		}
    		}    
    	}
    }
	
    /**
     * 
     * @param sensorList List<Sensor> - all available sensor
     * @param type int - interested type of sensor
     * @return Sensor - Sensor which found by type
     */
    private Sensor findSensorByType(List<Sensor> sensorList, int type) {
    	Sensor foundSensor = null;
    	for (Sensor sensor : sensorList) {
    		if (sensor.getType() == type) {
    			foundSensor = sensor;
    			break;
    		}
    	}
    	return foundSensor;
    }
	
    @SuppressLint("NewApi") 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		

        this.context = getApplicationContext();
        this.sensorActivity = this;

		Thread.setDefaultUncaughtExceptionHandler(new SensorUncaughtExceptionHandler(this.context));
        
		deviceNameTextView = (TextView)findViewById(R.id.deviceNameTextView);
		if (deviceNameTextView != null)
			deviceNameTextView.setText(extractDeviceDetails(false, false));

		// 2015.08.15.
		SensorSettings.loadPreferences(getApplicationContext());
		
		// Set Sensor Manager
        this.sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        
        List<Sensor> sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        List<Sensor> availableSensorList = new ArrayList<Sensor>(sensorList);
        
        if (android.os.Build.VERSION.SDK_INT >= 18) {
        	this.triggerListener = new TriggerListener();
        }	
        
        this.sensorDescriptorList.clear();

    	Sensor msensor = null;

        Resources resources = getResources();
        //String doesNotExists = getResources().getString(R.string.doesnot_exists_text);
        
        checkEsternalStoragePermission(this);
        
        sb = new StringBuffer();
        
        try {

            for (int type : this.sensorTypes) {
            	Sensor foundSensor = findSensorByType(availableSensorList, type);
            	if (foundSensor != null) {
            		availableSensorList.remove(foundSensor);
                	String text = SensorUtils.extractSensorName(resources, type);
                	SensorDescriptor sensorDescriptor = new SensorDescriptor();
                	boolean isSDKVersionOK =  isSensorExistsInSDKVersion(type);
                	if (isSDKVersionOK) {
                    	msensor = sensorManager.getDefaultSensor(type);
                    	if (msensor != null) {
                        	if (msensor.getType() == foundSensor.getType()) {
                        		String sensorTitle = SensorUtils.constructSensorTitle(text, type);
                        		String sensorName = SensorUtils.constructSensorName(msensor);
                        		String sensorDetails = SensorUtils.constructSensorDetails(msensor);
                        		
                            	setupDescriptor(sensorDescriptor, 0, sensorTitle, sensorName, sensorDetails, type, true);
                            	
                            	boolean isSuccess = false;
                            	/*
                        		int reportingMode = msensor.getReportingMode();
                        		if (reportingMode == Sensor.REPORTING_MODE_ON_CHANGE) {
                       				if (triggerListener != null) {
                       					isSuccess = sensorManager.requestTriggerSensor(triggerListener, msensor);
                       				}
                        		}
                            	else {
                            		isSuccess = sensorManager.registerListener(sensorEventListener, msensor, SensorManager.SENSOR_DELAY_NORMAL);
                            	}	
                            	*/
                            	isSuccess = false;
                            	if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
                            		if (android.os.Build.VERSION.SDK_INT >= 18) {
                            			if (triggerListener != null) {
                            				isSuccess = sensorManager.requestTriggerSensor(triggerListener, msensor);
                            			}
                            		} 
                            	}
                            	else {
                            		isSuccess = sensorManager.registerListener(sensorEventListener, msensor, SensorManager.SENSOR_DELAY_NORMAL);
                            	}	                        	
                            	if (!isSuccess) {
                                	setupDescriptor(sensorDescriptor, 1, "" + ((msensor != null) ? msensor.getType() : "N/A") + " " + text, extractSensorUnregMess(msensor.getType()), "",  ((msensor != null) ? msensor.getType() : -1), false);
                            	}	
                            	else {
                                	sensorDescriptor.setAvailable(true);
                            	}
                            	// add to list
                            	if (sensorsShowOptionKind.equals(SensorsShowOptionKind.ALL_SENSORS)) {
                            		this.sensorDescriptorList.add(sensorDescriptor);
                            	}
                            	else if (sensorsShowOptionKind.equals(SensorsShowOptionKind.AVAILABLE_SENSORS)) {
                            		if (sensorDescriptor.isAvailable()) {
                                		this.sensorDescriptorList.add(sensorDescriptor);
                            		}
                            	}
                        	}
                        	else {
                        		SDCardManager.debug("Type of default sensor and found available sensor is different!");
                        	}
                    	}
                    	else {
                    		SDCardManager.debug("No default sensor of available sensor type " + type);
                    	}
                	}
                	else {
                		SDCardManager.debug("SDK Version is not matched!");
                	}
            	}
            	else {
                	String text = SensorUtils.extractSensorName(resources, type);
                	SensorDescriptor sensorDescriptor = new SensorDescriptor();
               		String sensorTitle = SensorUtils.constructSensorTitle(text, type);
                  	setupDescriptor(sensorDescriptor, 1, sensorTitle, extractSensorUnregMess(type), "", type, false);
                	// add to list
                	if (sensorsShowOptionKind.equals(SensorsShowOptionKind.ALL_SENSORS)) {
                		this.sensorDescriptorList.add(sensorDescriptor);
                	}
                	else if (sensorsShowOptionKind.equals(SensorsShowOptionKind.AVAILABLE_SENSORS)) {
                		if (sensorDescriptor.isAvailable()) {
                    		this.sensorDescriptorList.add(sensorDescriptor);
                		}
                	}
                	SDCardManager.debug("Sensor type not found " + sensorDescriptor.getTitle());
            	}
            }	

            SDCardManager.debug("Remained Sensors " + availableSensorList.size());
            
            for (Sensor sensor : availableSensorList) {
            	String text = SensorUtils.extractSensorName(resources, sensor.getType());
            	SensorDescriptor sensorDescriptor = new SensorDescriptor();
            	boolean isSDKVersionOK =  isSensorExistsInSDKVersion(sensor.getType());
            	if (isSDKVersionOK) {
                	msensor = sensorManager.getDefaultSensor(sensor.getType());
                	if (msensor != null) {
                    	if (msensor.getType() == sensor.getType()) {
                    		String sensorTitle = SensorUtils.constructSensorTitle(text, msensor.getType());
                    		String sensorName = SensorUtils.constructSensorName(msensor);
                    		String sensorDetails = SensorUtils.constructSensorDetails(msensor);
                    		
                        	setupDescriptor(sensorDescriptor, 0, sensorTitle, sensorName, sensorDetails, msensor.getType(), true);
                        	
                        	boolean isSuccess = false;
                        	/*
                    		int reportingMode = msensor.getReportingMode();
                    		if (reportingMode == Sensor.REPORTING_MODE_ON_CHANGE) {
                   				if (triggerListener != null) {
                   					isSuccess = sensorManager.requestTriggerSensor(triggerListener, msensor);
                   				}
                    		}
                        	else {
                        		isSuccess = sensorManager.registerListener(sensorEventListener, msensor, SensorManager.SENSOR_DELAY_NORMAL);
                        	}	
                        	*/
                        	isSuccess = false;
                        	if (msensor.getType() == Sensor.TYPE_SIGNIFICANT_MOTION) {
                        		if (android.os.Build.VERSION.SDK_INT >= 18) {
                        			if (triggerListener != null) {
                        				isSuccess = sensorManager.requestTriggerSensor(triggerListener, msensor);
                        			}
                        		} 
                        	}
                        	else {
                        		isSuccess = sensorManager.registerListener(sensorEventListener, msensor, SensorManager.SENSOR_DELAY_NORMAL);
                        	}	                        	
                        	if (!isSuccess) {
                            	setupDescriptor(sensorDescriptor, 1, "" + ((msensor != null) ? msensor.getType() : "N/A") + " " + text, extractSensorUnregMess(msensor.getType()), "",  ((msensor != null) ? msensor.getType() : -1), false);
                        	}	
                        	else {
                            	sensorDescriptor.setAvailable(true);
                        	}
                        	// add to list
                        	if (sensorsShowOptionKind.equals(SensorsShowOptionKind.ALL_SENSORS)) {
                        		this.sensorDescriptorList.add(sensorDescriptor);
                        	}
                        	else if (sensorsShowOptionKind.equals(SensorsShowOptionKind.AVAILABLE_SENSORS)) {
                        		if (sensorDescriptor.isAvailable()) {
                            		this.sensorDescriptorList.add(sensorDescriptor);
                        		}
                        	}
                    	}
                    	else {
                    		SDCardManager.debug("Type of default sensor and found available sensor is different!");
                    	}
                	}
                	else {
                		SDCardManager.debug("No default sensor of available sensor type " + ((msensor != null) ? msensor.getType() : "N/A"));
                	}
            	}
            	else {
            		SDCardManager.debug("SDK Version is not matched");
            	}
            	
            }
        	
        	
        }
        catch (Exception e) {
        	SDCardManager.createErrorLog(this.getApplicationContext(), e);
        	Toast.makeText(this.getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
        
        
        /* sort if contains enough items
        if (this.sensorDescriptorList.size() > 1) {
        	Collections.sort(this.sensorDescriptorList, new Comparator<SensorDescriptor>() {
				@Override
				public int compare(SensorDescriptor lhs, SensorDescriptor rhs) {
					return lhs.compareTo(rhs);
				}
        	});
        }
        captureSensorsList(sb, "Ordered Captured sensors", this.sensorDescriptorList);
        */
        
        this.numberOfAllSensors =  (this.sensorDescriptorList != null) ? this.sensorDescriptorList.size() : 0;
        // check
        this.numberOfAvailableSensors = 0;
        // check sensors
       	for (SensorDescriptor sensorDescriptor : this.sensorDescriptorList) {
   			if (sensorDescriptor.isAvailable()) {
   				this.numberOfAvailableSensors++;
   			}
        }
        
        // change title
        String title = (String) this.getTitle();
        availableTitle = title + " #" + this.numberOfAvailableSensors + "/" + this.numberOfAllSensors;
        this.setTitle(availableTitle);
        
        // unregister sensors
        for (Sensor sensor : availableSensorList) {
        	sensorManager.unregisterListener(sensorEventListener, sensor);
        }
        
        availableSensorList = null;
        msensor = null;
        sensorManager = null;
        
     
        final ListView sensorsListView = (ListView) findViewById(R.id.SensorsListView);
        sensorsListView.setAdapter(new SensorsListViewAdapter(this, sensorDescriptorList));
        
        sensorsListView.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
        		Object o = sensorsListView.getItemAtPosition(position);
        		if (o != null) {
        			SensorDescriptor sensorDescriptor = (SensorDescriptor)o;
        			if (sensorDescriptor.getImageNumber() == 0) {
        				selectedType = sensorDescriptor.getType();
        				// start the Map Selector
        		        Intent intent = new Intent();
        		        intent.setClass(context, SensorValues.class);
        		        // start with download map mode
        	        	passCommonInfo(sensorActivity, intent, new Long(selectedType), new Long(0), new Long(0));
        		        startActivity(intent);      
        			}	
        		}
        	}  
        });
    	
    }
	
		
	class SensorListener implements SensorEventListener {
		public void onSensorChanged(SensorEvent event) {
        }

		@Override
		public void onAccuracyChanged(Sensor sensor, int value) {
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
	}
    
    @Override
    protected void onResume() {
        super.onResume();
    }
 
    @Override
    protected void onStart() {
    	super.onStart();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
    }
    
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.optionsmenu, menu);
		return true;
	}
    
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent();
		
		switch (item.getItemId()) {
		case R.id.itemAbout:
	        intent = new Intent();
	        intent.setClass(getApplicationContext(), Info.class);
	        startActivity(intent);      
			return true;
		case R.id.sensor_itemSettings:
			intent = new Intent();
			intent.setClass(getApplicationContext(), SensorPreferenceSettings.class);
			startActivity(intent);      
			return true;
		case R.id.sensor_itemSave :
			if (SDCardManager.isStoreDebugLog) {
				SDCardManager.createFileForStringBuffer(this.getApplicationContext(), "Sensor_log.txt", SDCardManager.log);
				SDCardManager.log.delete(0, SDCardManager.log.length());
			}	
			saveSensorList();
			break;
		}
		return true;
	}
 
	
	 @SuppressLint("NewApi") 
	 public String extractDeviceDetails(boolean inDetails, boolean withSerial) {
		String deviceName = "";
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			deviceName = deviceNameToCapitalize(model);
		} 
		else {
			deviceName = deviceNameToCapitalize(manufacturer) + " " + model;
		}
		if (inDetails) {
			deviceName += "\n Hardware:" + Build.HARDWARE;
			deviceName += " (" + Build.CPU_ABI + ")\n";
			deviceName += " Board: " + Build.BOARD + "\n";
			deviceName += " Bootloader: " + Build.BOOTLOADER + "\n";
			deviceName += " Band: " + Build.BRAND + "\n";
			deviceName += " Build ID: " + Build.DISPLAY + "\n";
			deviceName += " Board: " + Build.BOARD + "\n";
			deviceName += " Type: " + Build.TYPE + "\n";
			deviceName += " Time: " + Build.TIME + "\n";
		}
		
		if (withSerial) {
			if (android.os.Build.VERSION.SDK_INT >= 9) {
				deviceName += " Sn:" +Build.SERIAL+"\n";
			}	
		}
		return deviceName;
	 }


	 private String deviceNameToCapitalize(String s) {
	   if (s == null || s.length() == 0) {
	     return "";
	   }
	   char first = s.charAt(0);
	   if (Character.isUpperCase(first)) {
	     return s;
	   } 
	   else {
	     return Character.toUpperCase(first) + s.substring(1);
	   }
	 } 

	 private String extractAndroidVersion() {
		 String ver = null;
		 StringBuilder builder = new StringBuilder();
		 builder.append("Android ").append(Build.VERSION.RELEASE);

		 Field[] fields = Build.VERSION_CODES.class.getFields();
		 for (Field field : fields) {
		     String fieldName = field.getName();
		     int fieldValue = -1;

		     try {
		         fieldValue = field.getInt(new Object());
		     } 
		     catch (IllegalArgumentException e) {
		         e.printStackTrace();
		     } 
		     catch (IllegalAccessException e) {
		         e.printStackTrace();
		     } 
		     catch (NullPointerException e) {
		         e.printStackTrace();
		     }

		     if (fieldValue == Build.VERSION.SDK_INT) {
		         builder.append(" ");
		         builder.append(fieldName);
		         builder.append(" ");
		         builder.append("SDK#").append(fieldValue);
		     }
		 }

		 ver = builder.toString();
		 builder = null;
		 return ver;
	 }
	 
	 private void saveSensorList() {
		 String fileName = "Sensors-" + Build.MODEL + ".txt";
		 StringBuffer list = new StringBuffer();
		 // device name and how many sensors are available
		 if (availableTitle != null)
			 list.append(extractDeviceDetails(true, true) + " built-in sensors (" + availableTitle + ") \n\r ");
		 else
			 list.append(extractDeviceDetails(true, true) + " built in sensors \n\r ");
		 // android version
		 String androidVer = extractAndroidVersion();
		 if (!androidVer.equals(""))
			 list.append(androidVer + "\n\r ");
		 list.append(" \n\r ");
		 // sensor availability
		 for (SensorDescriptor sensorDescriptor : sensorDescriptorList) {
			 String line = (sensorDescriptor.isAvailable()) ? "√ " : "X ";
			 line +=  sensorDescriptor.getTitle() + " : " + sensorDescriptor.getName() + "\n\r " +  ((sensorDescriptor.getDetails().trim() == "") ? "" : sensorDescriptor.getDetails() + "\n\r "); 
			 list.append(line);
		 }
		 list.append(" \n\r ");
		 // apk version and code
		 String ver = SensorUtils.extractVersionCodeAndName(this);
		 list.append("by Sensors v" + ver + " \n\r ");
		 list.append(getResources().getString(R.string.appmain_copyright) + "\n\r ");
		 list.append(" \n\r ");
		 String savePath = SDCardManager.createFileForStringBuffer(context, fileName, list);
		 if (savePath != null) {
			 Toast.makeText(context, savePath, Toast.LENGTH_LONG).show();
		 }
		 list = null;
	 }
	 
}