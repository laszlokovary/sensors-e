package com.lkovari.mobile.apps.sensors;

/**
 * 
 * @author lkovari
 *
 */
public enum SensorsShowOptionKind {
	ALL_SENSORS,
	AVAILABLE_SENSORS;
}
