package com.lkovari.mobile.apps.sensors;

import java.io.Serializable;


public class SensorDescriptor implements Serializable, Comparable<SensorDescriptor> {
	private static final long serialVersionUID = -4327199377240961483L;
	private String title = null;
	private String name = null;
	private String details = null;
	private int imageNumber = 0;
	private Integer type = -1;
	private boolean isAvailable = false;

	@Override
	public int compareTo(SensorDescriptor another) {
		return this.type.compareTo(another.getType());
	}
	

	@Override
	public boolean equals(Object o) {
		int res = 0;
		if (o != null && o instanceof SensorDescriptor) {
			SensorDescriptor sd1 = (SensorDescriptor)o;
			res = sd1.compareTo(sd1);
		}
		return res == 0;
	}

	@Override
	public String toString() {
		return "" + this.type + " " + this.title + " " + this.name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getType() {
		return type;
	}
	
	public void setType(Integer type) {
		this.type = type;
	}
	
	public boolean isAvailable() {
		return isAvailable;
	}
	
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	public void setImageNumber(int imageNumber) {
		this.imageNumber = imageNumber;
	}
	public int getImageNumber() {
		return imageNumber;
	}

	
}
