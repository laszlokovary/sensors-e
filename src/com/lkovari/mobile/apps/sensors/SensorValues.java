package com.lkovari.mobile.apps.sensors;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorAdditionalInfo;
import android.hardware.SensorEvent;
import android.hardware.SensorEventCallback;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

/**
 * 
 * @author lkovari
 *
 */
public class SensorValues extends Activity implements SensorEventListener {
	// 01/02/2017 
	private SensorValueCapturing sensorValueCapturing = null;
	
	private SensorManager sensorManager = null;

	Sensor allSensors = null;
	
	Sensor accelerometerSensor = null;
	Sensor ambientTemperatureSensor = null;
	Sensor gravitySensor = null;
	Sensor gyroscopeSensor = null;
	Sensor lightSensor = null;
	Sensor linearAccelerationSensor = null;
	Sensor magneticfieldSensor = null;
	Sensor orientationSensor = null;
	Sensor pressureSensor = null;
	Sensor proximitySensor = null;
	Sensor relativeHumiditySensor = null;
	Sensor rotationVectorSensor = null;
	Sensor temperatureSensor = null;
	
	Sensor uncalibratedMagneticFieldSensor = null;
	Sensor geomagneticRotationVectorSensor = null;
	Sensor gameRotationVectorSensor = null;
	Sensor uncalibratedGyroscopeeSensor = null;
	Sensor significantMotionSensor = null;
	Sensor stepCounterSensor = null;
	Sensor stepDetectorSensor = null;
	Sensor heartRateSensor = null;
	
	private TextView sensorTextView;
	private TextView sensorAccuracyTitle;
	private TextView sensorValueX;
	private TextView sensorValueY;
	private TextView sensorValueZ;
	private TextView sensorValueW;
	private TextView sensorValuebX;
	private TextView sensorValuebY;
	private TextView sensorValuebZ;
	private Context context;

	private final float kalmanFilteringFactor = 0.1f;
	private float[] gravity = {0,0,0};
	private float lastAccelerometerX = new Float(0.0);
	private float lastAccelerometerY = new Float(0.0);
	private float lastAccelerometerZ = new Float(0.0);
	private final float alpha = (float) 0.8;
	
	private float[] mags = new float[3];
	private float[] umags = new float[6];
	private float[] accels = new float[3];
	private float[] orient = new float[3];
	private float presure = Float.MIN_VALUE;
	private float proximity = Float.MIN_VALUE;
	private float temp = Float.MIN_VALUE;

	private float sumGravityX = new Float(0.0);
	private float sumGravityY = new Float(0.0);
	private float sumGravityZ = new Float(0.0);
	private float sumLinearAccX = new Float(0.0);
	private float sumLinearAccY = new Float(0.0);
	private float sumLinearAccZ = new Float(0.0);
	private float sumGyroscopeX = new Float(0.0);
	private float sumGyroscopeY = new Float(0.0);
	private float sumGyroscopeZ = new Float(0.0);
	private float maxmAccelerometerG = -1.0F;
	private float sumAccelerometerX = new Float(0.0);
	private float sumAccelerometerY = new Float(0.0);
	private float sumAccelerometerZ = new Float(0.0);
	private float sumLight = new Float(0.0);
	private float sumMagneticFieldX = new Float(0.0);
	private float sumMagneticFieldY = new Float(0.0);
	private float sumMagneticFieldZ = new Float(0.0);
	private float sumMagneticUFieldX = new Float(0.0);
	private float sumMagneticUFieldY = new Float(0.0);
	private float sumMagneticUFieldZ = new Float(0.0);
	private float sumOrientationAzimuth = new Float(0.0);
	private float sumOrientationPitch = new Float(0.0);
	private float sumOrientationRoll = new Float(0.0);
	private float sumPresure = new Float(0);
	private float sumProximity = new Float(0);
	private float sumTemperature = new Float(0);
	
	private int cntLinearAcc = 0;
	private int cntGravity = 0;
	private int cntAccelerometer = 0;
	private int cntGyroscope = 0;
	private int cntMagneticField = 0;
	private int cntOrientation = 0;
	private int cntPresure = 0;
	private int cntTemperature = 0;
	private int selectedType = -1;
	private float stepOffset = 0.0F;
	private int stepDetector = 0;
	private int heartRate = 0;
	private float heartBeat = 0;
	private float motionDetect = 0;
	private float stationaryDetect = 0;
	private float privateBase = 0;	
	private float posedof1 = 0;
	private float posedof2 = 0;
	private float posedof3 = 0;
	private float posedof5 = 0;
	private float posedof6 = 0;
	private float posedof7 = 0;

	
	// 02/08/2014
	private Sensor sensor = null;
	private TextView sensorNameTextView = null;
	private TextView sensorPowerTextView = null;
	private TextView sensorMinDelayTextView = null;
	private TextView sensorResolutionTextView = null;
	private TextView sensorMaxRangeTextView = null;

	private SVTriggerListener triggerListener = null;
	private Date timeStamp = new Date();
	// 01/02/2017 
	private String internalTemperature = null;
	

	/**
	 * 01/02/2017 
	 * @author lkovari
	 *
	 */
	@TargetApi(24) 
	@SuppressLint("NewApi") 
	class SensorValueCapturing extends SensorEventCallback {
		@Override
		public void onSensorChanged(SensorEvent event) {
			internalTemperature = null;
			handlingSensorChanged(event);
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			if (sensor.getType() != selectedType) {
				return;
			}
			showAccuracyValues(sensor.getType(), accuracy);
		}

		@Override
		public void onFlushCompleted(Sensor sensor) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onSensorAdditionalInfo(SensorAdditionalInfo info) {
			if (info != null) {
				switch (info.type) {
					case SensorAdditionalInfo.TYPE_FRAME_BEGIN : {
						break;
					}
					case SensorAdditionalInfo.TYPE_FRAME_END : {
						break;
					}
					case SensorAdditionalInfo.TYPE_INTERNAL_TEMPERATURE : {
						float[] floatValues = info.floatValues;
						if (floatValues != null && floatValues.length > 0) {
							internalTemperature = String.format("Int.Temp %.1f%C", floatValues[0]);
						}
						break;
					}
					case SensorAdditionalInfo.TYPE_SAMPLING : {
						break;
					}
					case SensorAdditionalInfo.TYPE_SENSOR_PLACEMENT : {
						break;
					}
					case SensorAdditionalInfo.TYPE_UNTRACKED_DELAY : {
						break;
					}
					case SensorAdditionalInfo.TYPE_VEC3_CALIBRATION : {
						break;
					}
				}
			}
		}
	}
	
	
	@SuppressLint("NewApi") 
	class SVTriggerListener extends TriggerEventListener {
		@Override
		public void onTrigger(TriggerEvent event) {
			String triggerValue = "Has no trigger";
			if (event.values.length > 0) {
				float trigger = event.values[0];
				triggerValue = ((trigger == 1) || (trigger == 1.0)) ? "Has trigger" : "Has no trigger";
			}
			showValues(Sensor.TYPE_SIGNIFICANT_MOTION, triggerValue, "", "", "", "", "", "");
		}
	}
	
	/**
	 * 
	 * @param event SensorEvent
	 */
	@SuppressLint("NewApi") 
	private void handlingSensorChanged(SensorEvent event) {
		int type = event.sensor.getType();
		if (type != selectedType) {
			return;
		}
		
		if (type == Sensor.TYPE_ACCELEROMETER) {
			cntAccelerometer++;
			sumAccelerometerX += event.values[0];
			sumAccelerometerY += event.values[1];
			sumAccelerometerZ += event.values[2];

			if (cntAccelerometer == 5) {
				float avgAccelerationValueX = sumAccelerometerX / cntAccelerometer;
				float avgAccelerationValueY = sumAccelerometerY / cntAccelerometer;
				float avgAccelerationValueZ = sumAccelerometerZ / cntAccelerometer;

				//Total acceleration will be sqrt(x^2+y^2+z^2)
	            float gforce  = ((float)Math.sqrt(avgAccelerationValueX * avgAccelerationValueX + avgAccelerationValueY * avgAccelerationValueY + avgAccelerationValueZ * avgAccelerationValueZ) / SensorManager.GRAVITY_EARTH);
	            if (gforce > maxmAccelerometerG) {
	            	maxmAccelerometerG = gforce;
	            }
	            
				String accelerationValueX = String.format("%+5.2f", avgAccelerationValueX);
				String accelerationValueY = String.format("%+5.2ff", avgAccelerationValueY);
				String accelerationValueZ = String.format("%+5.2f", avgAccelerationValueZ);
				String accelerationValueW = null;
				gforce = Math.abs(gforce);
				accelerationValueW = String.format("%+8.5f max %+8.5f", gforce, maxmAccelerometerG);
					
				showValues(type, accelerationValueX, accelerationValueY, accelerationValueZ, accelerationValueW, "", "", "");
                sumAccelerometerX = 0;
                sumAccelerometerY = 0;
                sumAccelerometerZ = 0;
				cntAccelerometer = 0;
			}
		}
		else if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			temp = event.values[0];
			String temperatureValue1 = String.format("%+5.2f", event.values[0]);
			String temperatureValue2 = String.format("%+5.2f", event.values[1]);
			String temperatureValue3 = String.format("%+5.2f", event.values[2]);
			showValues(type, temperatureValue1, temperatureValue2, temperatureValue3, "", "", "", "");
		}
		else if (type == Sensor.TYPE_GRAVITY) {
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			
			cntGravity++;
			sumGravityX += x;
			sumGravityY += y;
			sumGravityZ += z;
			if (cntGravity == 5) {
				String gravityValueX = String.format("%+5.2f", sumGravityX / cntGravity);
				String gravityValueY = String.format("%+5.2ff", sumGravityY / cntGravity);
				String gravityValueZ = String.format("%+5.2f", sumGravityZ / cntGravity);
				showValues(type, gravityValueX, gravityValueY, gravityValueZ, "", "", "", "");
                sumGravityX = 0;
                sumGravityY = 0;
                sumGravityZ = 0;
				cntGravity = 0;
			}
		}
		else if (type == Sensor.TYPE_GYROSCOPE) {
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			sumGyroscopeX += x;
			sumGyroscopeY += y;
			sumGyroscopeZ += z;
			cntGyroscope++;
			if (cntGyroscope == 5) {
				String gyroscopeValueX = String.format("%+5.2f", sumGyroscopeX / cntGyroscope);
				String gyroscopeValueY = String.format("%+5.2ff", sumGyroscopeY / cntGyroscope);
				String gyroscopeValueZ = String.format("%+5.2f", sumGyroscopeZ / cntGyroscope);
				showValues(type, gyroscopeValueX, gyroscopeValueY, gyroscopeValueZ, "", "", "", "");
                sumGyroscopeX = 0;
                sumGyroscopeY = 0;
                sumGyroscopeZ = 0;
				cntGyroscope = 0;
			}
		}
		else if (type == Sensor.TYPE_LIGHT) {
			String lightValue = String.format("%5.1f", event.values[0]);
			showValues(type, lightValue, "", "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_LINEAR_ACCELERATION) {
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			
			cntLinearAcc++;
			sumLinearAccX += x;
			sumLinearAccY += y;
			sumLinearAccZ += z;
			
			if (cntLinearAcc == 5) {
				float avgLinAccelerationValueX = sumLinearAccX / cntLinearAcc;
				float avgLinAccelerationValueY = sumLinearAccY / cntLinearAcc;
				float avgLinAccelerationValueZ = sumLinearAccZ / cntLinearAcc;
				
				float gforce  = ((float)Math.sqrt(avgLinAccelerationValueX * avgLinAccelerationValueX + avgLinAccelerationValueY * avgLinAccelerationValueY + avgLinAccelerationValueZ * avgLinAccelerationValueZ) / SensorManager.GRAVITY_EARTH);
				
	            String linearAccelerationValueX = String.format("%+5.2f", avgLinAccelerationValueX);
				String linearAccelerationValueY = String.format("%+5.2f", avgLinAccelerationValueY);
				String linearAccelerationValueZ = String.format("%+5.2f", avgLinAccelerationValueZ);
				String linearAccelerationValueW = null;
				if (gforce > 0.0) {
					linearAccelerationValueW = String.format("%+8.4f", gforce);
				}	
				showValues(type, linearAccelerationValueX, linearAccelerationValueY, linearAccelerationValueZ, "", "", "", "");
                sumLinearAccX = 0;
                sumLinearAccY = 0;
                sumLinearAccZ = 0;
				cntLinearAcc = 0;
			}
		}
		else if ( type == Sensor.TYPE_MAGNETIC_FIELD) {
			mags = event.values;
			cntMagneticField++;
			sumMagneticFieldX += mags[0];
			sumMagneticFieldY += mags[1];
			sumMagneticFieldZ += mags[2];
			
			if (cntMagneticField == 5) {
				float avgMagneticValueX = sumMagneticFieldX / cntMagneticField;
				float avgMagneticValueY = sumMagneticFieldY / cntMagneticField;
				float avgMagneticValueZ = sumMagneticFieldZ / cntMagneticField;
				
				String magneticFieldValueX = String.format("%+8.3f", avgMagneticValueX);
				String magneticFieldValueY = String.format("%+8.3f", avgMagneticValueY);
				String magneticFieldValueZ = String.format("%+8.3f", avgMagneticValueZ);
				
				showValues(type, magneticFieldValueX, magneticFieldValueY, magneticFieldValueZ, "", "", "", "");
                
				cntMagneticField = 0;
                sumMagneticFieldX = 0;
                sumMagneticFieldY = 0;
                sumMagneticFieldZ = 0;
			}
		}
		else if (type == Sensor.TYPE_ORIENTATION) {
			/*
			float[] rotationMatrix = generateRotationMatrix(avgAccelerationValues, avgMagneticValues);
			float[] orientationValues = determineOrientation(rotationMatrix);
			*/
			cntOrientation++;
			sumOrientationAzimuth += event.values[0];
			sumOrientationPitch += event.values[1];
			sumOrientationRoll += event.values[2];
			if (cntOrientation == 10) {
				String orientationValueX = String.format("%+6.2f", sumOrientationAzimuth / cntOrientation);
				String orientationValueY = String.format("%+6.2f", sumOrientationPitch / cntOrientation);
				String orientationValueZ = String.format("%+6.2f", sumOrientationRoll / cntOrientation);
				showValues(type, orientationValueX, orientationValueY, orientationValueZ, "", "", "", "");
				cntOrientation = 0;
				sumOrientationAzimuth = 0;
				sumOrientationPitch = 0;
				sumOrientationRoll = 0;
			}
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			presure = event.values[0];
			cntPresure++;
			sumPresure += presure;
			if (cntPresure == 5) {
//				String pressureText = getResources().getString(R.string.presure_text_title);
				float currentPresure = sumPresure / cntPresure;
				float currentAltitude = SensorManager.getAltitude(currentPresure, (float) SensorSettings.SENSOR_PRESURE_FOR_ALTIMETER_QNH);
				currentAltitude = Math.abs(currentAltitude);
				String presureValue = String.format("%+5.2f", currentPresure);
				String altitudeValue = String.format("%+4.1f", currentAltitude);
				showValues(type, presureValue, altitudeValue, "", "", "", "", "");
				cntPresure = 0;
				sumPresure = 0;
			}
		}
		else if (type == Sensor.TYPE_PROXIMITY) {
			proximity = event.values[0];
//			String distanceText = getResources().getString(R.string.proximity_distance);
			String proximityValue = String.format("%+5.2f", proximity);
			showValues(type, proximityValue, "", "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_RELATIVE_HUMIDITY) {
			float humidity = event.values[0];
			String relativeHumidityValue = String.format("%3.2f", humidity);
			showValues(type, relativeHumidityValue, "", "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_ROTATION_VECTOR) {
			String rvText = "";
			/*
			float[] rotationValues = event.values.clone();
			float[] rotationMatrix = new float[16];
			SensorManager.getRotationMatrixFromVector(rotationMatrix, rotationValues);
			float [] orientationValues = determineOrientation(rotationMatrix);	
			*/
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			String rotationVectorValueX = String.format("%+6.2f", x);
			String rotationVectorValueY = String.format("%+6.2f", y);
			String rotationVectorValueZ = String.format("%+6.2f", z);
			String rotationVectorValueW = "";
			if (event.values.length == 4) {
				float cos = event.values[3];
				rotationVectorValueW = String.format("%+6.2f", cos);
			}
			showValues(type, rotationVectorValueX, rotationVectorValueY, rotationVectorValueZ, rotationVectorValueW, "", "", "");
		}
		else if (type == Sensor.TYPE_TEMPERATURE) {
			temp = event.values[0];
			cntTemperature++;
			sumTemperature+= temp;
			if (cntTemperature == 10) {
				String temperatureValue = String.format("%6.2f", sumTemperature / cntTemperature);
				showValues(type, temperatureValue, "", "", "", "", "", "");
				cntTemperature = 0;
				sumTemperature = 0;
			}
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			String magneticFieldValueX = String.format("%+8.3f", event.values[0]);
			String magneticFieldValueY = String.format("%+8.3f", event.values[1]);
			String magneticFieldValueZ = String.format("%+8.3f", event.values[2]);
			String magneticFieldValuebX = "";
			String magneticFieldValuebY = "";
			String magneticFieldValuebZ = "";
			if (event.values.length >= 4) {
				magneticFieldValuebX = String.format("%+8.3f", event.values[3]);
			}
			if (event.values.length >= 5) {
				magneticFieldValuebY = String.format("%+8.3f", event.values[4]);
			}
			if (event.values.length >= 6) {
				magneticFieldValuebZ = String.format("%+8.3f", event.values[5]);
			}
			showValues(type, magneticFieldValueX, magneticFieldValueY, magneticFieldValueZ, "", magneticFieldValuebX, magneticFieldValuebY, magneticFieldValuebZ);
                
		}
		else if (type == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			String rvText = "";
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			String rotationVectorValueX = String.format("%+6.2f", x);
			String rotationVectorValueY = String.format("%+6.2f", y);
			String rotationVectorValueZ = String.format("%+6.2f", z);
			String rotationVectorValueW = "";
			if (event.values.length == 4) {
				float cos = event.values[3];
				rotationVectorValueW = String.format("%+6.2f", cos);
			}
			showValues(type, rotationVectorValueX, rotationVectorValueY, rotationVectorValueZ, rotationVectorValueW, "", "", "");
		}
		else if (type == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			String rvText = "";
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			String rotationVectorValueX = String.format("%+6.2f", x);
			String rotationVectorValueY = String.format("%+6.2f", y);
			String rotationVectorValueZ = String.format("%+6.2f", z);
			String rotationVectorValueW = "";
			if (event.values.length == 4) {
				float cos = event.values[3];
				rotationVectorValueW = String.format("%+6.2f", cos);
			}
			showValues(type, rotationVectorValueX, rotationVectorValueY, rotationVectorValueZ, rotationVectorValueW, "", "", "");
		}
		else if (type == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			String gyroscopeValueX = String.format("%+5.2f", x);
			String gyroscopeValueY = String.format("%+5.2ff", y);
			String gyroscopeValueZ = String.format("%+5.2f", z);
			String gyroscopeEDAValueX = "";
			String gyroscopeEDAValueY = "";
			String gyroscopeEDAValueZ = "";
			if (event.values.length >= 4) {
				gyroscopeEDAValueX = String.format("%+5.2f", event.values[3]);
			}
			if (event.values.length >= 5) {
				gyroscopeEDAValueY = String.format("%+5.2f", event.values[4]);
			}
			if (event.values.length >= 6) {
				gyroscopeEDAValueZ = String.format("%+5.2f", event.values[5]);
			}

			showValues(type, gyroscopeValueX, gyroscopeValueY, gyroscopeValueZ, "", gyroscopeEDAValueX, gyroscopeEDAValueY, gyroscopeEDAValueZ);
		}
		else if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
			float trigger = event.values[0];
			String triggerValue = (trigger == 1.0) ? "Has trigger" : "has no trigger";
			showValues(type, triggerValue, "", "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_STEP_COUNTER) {
			if (stepOffset == 0) {
				stepOffset = event.values[0];
			}
			String stepCountValue = Float.toString(event.values[0] - stepOffset);
			showValues(type, stepCountValue, "", "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_STEP_DETECTOR) {
			String triggerValue = "-";
			String triggerValueTime = "-";
			if ((event != null) && (event.values != null)) {
				if (event.values.length >= 1) {
					if ((event.values[0] == 1.0f) || (event.values[0] == 1)) {
						triggerValue = "Has trigger";
					}			
				}
			}
			timeStamp.setTime(event.timestamp);
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM, Locale.getDefault());
			triggerValueTime = dateFormat.format(timeStamp);
			showValues(type, triggerValue, triggerValueTime, "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_HEART_RATE) {
			if (event.values[0] > 0) {
				heartRate = (int) event.values[0];
			}
			String heartRateValue = Integer.toString(heartRate);
			showValues(type, heartRateValue, "", "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_HEART_BEAT) {
			if (event.values[0] > 0) {
				heartBeat = (float) event.values[0];
			}
			String heartBeatValue = Float.toString(heartBeat);
			showValues(type, heartBeatValue, "", "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_MOTION_DETECT) {
			if (event.values[0] > 0) {
				motionDetect = (float) event.values[0];
			}
			String motionDetectValue = Float.toString(motionDetect);
			showValues(type, motionDetectValue, "", "", "", "", "", "");
		}
		else if (type == Sensor.TYPE_POSE_6DOF) {
			String posedof1Value = "";
			String posedof2Value = "";
			String posedof3Value = "";
			String posedof5Value = "";
			String posedof6Value = "";
			String posedof7Value = "";
			if (event.values.length > 0 && event.values[0] > 0) {
				posedof1 = (float) event.values[0];
				posedof1Value = Float.toString(posedof1);
			}
			else if (event.values.length > 1 && event.values[1] > 0) {
				posedof2 = (float) event.values[1];
				posedof2Value = Float.toString(posedof2);
			}
			else if (event.values.length > 2 && event.values[2] > 0) {
				posedof3 = (float) event.values[2];
				posedof3Value = Float.toString(posedof3);
			}
			else if (event.values.length > 3 && event.values[3] > 0) {
				posedof5 = (float) event.values[3];
				posedof5Value = Float.toString(posedof5);
			}
			else if (event.values.length > 4 && event.values[4] > 0) {
				posedof6 = (float) event.values[4];
				posedof6Value = Float.toString(posedof6);
			}
			else if (event.values.length > 5 && event.values[5] > 0) {
				posedof7 = (float) event.values[5];
				posedof7Value = Float.toString(posedof7);
			}
			
			showValues(type, posedof1Value, posedof2Value, posedof3Value, "", posedof5Value, posedof6Value, posedof7Value);
			/*
    		values[0]: x*sin(θ/2)
    		values[1]: y*sin(θ/2)
    		values[2]: z*sin(θ/2)
    		values[3]: cos(θ/2)
    		values[4]: Translation along x axis from an arbitrary origin.
    		values[5]: Translation along y axis from an arbitrary origin.
    		values[6]: Translation along z axis from an arbitrary origin.
    		values[7]: Delta quaternion rotation x*sin(θ/2)
    		values[8]: Delta quaternion rotation y*sin(θ/2)
    		values[9]: Delta quaternion rotation z*sin(θ/2)
    		values[10]: Delta quaternion rotation cos(θ/2)
    		values[11]: Delta translation along x axis.
    		values[12]: Delta translation along y axis.
    		values[13]: Delta translation along z axis.
    		values[14]: Sequence number
			 */
		}
		else if (type == Sensor.TYPE_STATIONARY_DETECT) {
			String stationary0Value = "";
			String stationary1Value = "";
			String stationary2Value = "";
			String stationary3Value = "";
			if (event.values.length > 0 && event.values[0] > 0) {
				float stationary0Float = (float) event.values[0];
				stationary0Value = Float.toString(stationary0Float);
			}	
			if (event.values.length > 1 && event.values[1] > 0) {
				float stationary1Float = (float) event.values[1];
				stationary1Value = Float.toString(stationary1Float);
			}	
			if (event.values.length > 2 && event.values[2] > 0) {
				float stationary2Float = (float) event.values[2];
				stationary2Value = Float.toString(stationary2Float);
			}	
			if (event.values.length > 3 && event.values[3] > 0) {
				float stationary3Float = (float) event.values[3];
				stationary3Value = Float.toString(stationary3Float);
			}
			showValues(type, stationary0Value, stationary1Value, stationary2Value, stationary3Value, "", "", "");
		}
		else if (type == SensorUtils.TYPE_TILT_DETECTOR) {
			String tilt0Value = "";
			String tilt1Value = "";
			String tilt2Value = "";
			String tilt3Value = "";
			if (event.values.length > 0 && event.values[0] > 0) {
				float tilt0Float = (float) event.values[0];
				tilt0Value = Float.toString(tilt0Float);
			}	
			if (event.values.length > 1 && event.values[1] > 0) {
				float tilt1Float = (float) event.values[1];
				tilt1Value = Float.toString(tilt1Float);
			}	
			if (event.values.length > 2 && event.values[2] > 0) {
				float tilt2Float = (float) event.values[2];
				tilt2Value = Float.toString(tilt2Float);
			}	
			if (event.values.length > 3 && event.values[3] > 0) {
				float tilt3Float = (float) event.values[3];
				tilt3Value = Float.toString(tilt3Float);
			}
			showValues(type, tilt0Value, tilt1Value, tilt2Value, tilt3Value, "", "", "");
		}
		else if (type == SensorUtils.TYPE_SCREEN_ORIENTATION_V3) {
			float screenOrientationDetect = 0;
			if (event.values[0] > 0) {
				screenOrientationDetect = (float) event.values[0];
			}
			String screenOrientationDetectValue = Float.toString(screenOrientationDetect);
			if (screenOrientationDetect == 255.0) {
				screenOrientationDetectValue = getResources().getString(R.string.device_orientation_horizontal);
			}
			else if (screenOrientationDetect == 0.0) {
				screenOrientationDetectValue = getResources().getString(R.string.device_orientation_up);
			}
			else if (screenOrientationDetect == 1.0) {
				screenOrientationDetectValue = getResources().getString(R.string.device_orientation_left);
			}
			else if (screenOrientationDetect == 3.0) {
				screenOrientationDetectValue = getResources().getString(R.string.device_orientation_right);
			}
			else if (screenOrientationDetect == 2.0) {
				screenOrientationDetectValue = getResources().getString(R.string.device_orientation_down);
			}
			showValues(type, screenOrientationDetectValue, "", "", "", "", "", "");
		}
		else if (type == SensorUtils.TYPE_GRIP_SENSOR_V512) {
			String grip_u_DetectValue = "";
			String grip_d_DetectValue = "";
			String grip_l_DetectValue = "";
			String grip_r_DetectValue = "";
			if (event.values.length > 0) {
				float grip_u_Detect = (float) event.values[0];
				if (event.values[0] > 0) {
					grip_u_DetectValue = getResources().getString(R.string.nogrip_text_title);
				}	
				else {
					grip_u_DetectValue = getResources().getString(R.string.grip_text_title);
				}
				grip_u_DetectValue +=  " " + Float.toString(grip_u_Detect);
			}	
			if (event.values.length > 1 && event.values[1] > 0) {
				float grip_d_Detect = (float) event.values[1];
				grip_d_DetectValue = Float.toString(grip_d_Detect);
			}	
			if (event.values.length > 2 && event.values[2] > 0) {
				float grip_l_Detect = (float) event.values[2];
				grip_l_DetectValue = Float.toString(grip_l_Detect);
			}	
			if (event.values.length > 3 && event.values[3] > 0) {
				float grip_r_Detect = (float) event.values[3];
				grip_r_DetectValue = Float.toString(grip_r_Detect);
			}
			showValues(type, grip_u_DetectValue, grip_d_DetectValue, grip_l_DetectValue, grip_r_DetectValue, "", "", "");
		}
		else if (type == SensorUtils.TYPE_RGB_IR_SENSOR_V1) {
			String rgbIr_cpl_Value = "";
			String rgbIr_r_Value = "";
			String rgbIr_g_Value = "";
			String rgbIr_b_Value = "";
			if (event.values.length > 0 && event.values[0] > 0) {
				float rgbIr_cpl_Detect = (float) event.values[0];
				rgbIr_cpl_Value = Float.toString(rgbIr_cpl_Detect);
			}	
			if (event.values.length > 1 && event.values[1] > 0) {
				float rgbIr_r_Detect = (float) event.values[1];
				rgbIr_r_Value = Float.toString(rgbIr_r_Detect);
			}	
			if (event.values.length > 2 && event.values[2] > 0) {
				float rgbIr_g_Detect = (float) event.values[2];
				rgbIr_g_Value = Float.toString(rgbIr_g_Detect);				
			}	
			if (event.values.length > 3 && event.values[3] > 0) {
				float rgbIr_b_Detect = (float) event.values[3];
				rgbIr_b_Value = Float.toString(rgbIr_b_Detect);
			}
			showValues(type, rgbIr_cpl_Value, rgbIr_r_Value, rgbIr_g_Value, rgbIr_b_Value, "", "", "");
		}
		else if (type == SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1) {
			String pickupGyro0Value = "";
			String pickupGyro1Value = "";
			String pickupGyro2Value = "";
			String pickupGyro3Value = "";
			
			if (event.values.length > 0 && event.values[0] > 0) {
				float intrGyro0Detect = (float) event.values[0];
				pickupGyro0Value = Float.toString(intrGyro0Detect);
			}
			if (event.values.length > 1 && event.values[1] > 0) {
				float intrGyro1Detect = (float) event.values[1];
				pickupGyro1Value = Float.toString(intrGyro1Detect);
			}
			if (event.values.length > 2 && event.values[2] > 0) {
				float intrGyro2Detect = (float) event.values[2];
				pickupGyro2Value = Float.toString(intrGyro2Detect);
			}
			if (event.values.length > 3 && event.values[3] > 0) {
				float intrGyro3Detect = (float) event.values[3];
				pickupGyro3Value = Float.toString(intrGyro3Detect);
			}
			showValues(type, pickupGyro0Value, pickupGyro1Value, pickupGyro2Value, pickupGyro3Value, "", "", "");
		}
		else if (type == SensorUtils.TYPE_HRMLED_IR_SENSOR_V1) {
			String hrmledIr0Value = "";
			String hrmledIr1Value = "";
			String hrmledIr2Value = "";
			String hrmledIr3Value = "";
			
			if (event.values.length > 0 && event.values[0] > 0) {
				float hrmledIr0Float = (float) event.values[0];
				hrmledIr0Value = Float.toString(hrmledIr0Float);
			}
			if (event.values.length > 1 && event.values[1] > 0) {
				float hrmledIr1Float = (float) event.values[1];
				hrmledIr1Value = Float.toString(hrmledIr1Float);
			}
			if (event.values.length > 2 && event.values[2] > 0) {
				float hrmledIr2Float = (float) event.values[2];
				hrmledIr2Value = Float.toString(hrmledIr2Float);
			}
			if (event.values.length > 3 && event.values[3] > 0) {
				float hrmledIr3Float = (float) event.values[3];
				hrmledIr3Value = Float.toString(hrmledIr3Float);
			}
			showValues(type, hrmledIr0Value, hrmledIr1Value, hrmledIr2Value, hrmledIr3Value, "", "", "");
		}
		else if (type == SensorUtils.TYPE_HRMLED_RED_SENSOR_V1) {
			String hrmledRed0Value = "";
			String hrmledRed1Value = "";
			String hrmledRed2Value = "";
			String hrmledRed3Value = "";
			
			if (event.values.length > 0 && event.values[0] > 0) {
				float hrmledRed0Float = (float) event.values[0];
				hrmledRed0Value = Float.toString(hrmledRed0Float);
			}
			if (event.values.length > 1 && event.values[1] > 0) {
				float hrmledRed1Float = (float) event.values[1];
				hrmledRed1Value = Float.toString(hrmledRed1Float);
			}
			if (event.values.length > 2 && event.values[2] > 0) {
				float hrmledRed2Float = (float) event.values[2];
				hrmledRed2Value = Float.toString(hrmledRed2Float);
			}
			if (event.values.length > 3 && event.values[3] > 0) {
				float hrmledRed3Float = (float) event.values[3];
				hrmledRed3Value = Float.toString(hrmledRed3Float);
			}
			showValues(type, hrmledRed0Value, hrmledRed1Value, hrmledRed2Value, hrmledRed3Value, "", "", "");
		}
		else if (type == SensorUtils.TYPE_PICK_UP_GESTURE) {
			String pickUpGesture0Value = "";
			String pickUpGesture1Value = "";
			String pickUpGesture2Value = "";
			String pickUpGesture3Value = "";
			if (event.values.length > 0 && event.values[0] > 0) {
				float pickUpGesture0Float = (float) event.values[0];
				pickUpGesture0Value = Float.toString(pickUpGesture0Float);
			}
			if (event.values.length > 1 && event.values[1] > 0) {
				float pickUpGesture1Float = (float) event.values[1];
				pickUpGesture1Value = Float.toString(pickUpGesture1Float);
			}
			if (event.values.length > 2 && event.values[2] > 0) {
				float pickUpGesture2Float = (float) event.values[2];
				pickUpGesture2Value = Float.toString(pickUpGesture2Float);
			}
			if (event.values.length > 3 && event.values[3] > 0) {
				float pickUpGesture3Float = (float) event.values[3];
				pickUpGesture3Value = Float.toString(pickUpGesture3Float);
			}
			showValues(type, pickUpGesture0Value, pickUpGesture1Value, pickUpGesture2Value, pickUpGesture3Value, "", "", "");
		}
	}
	
	private String setupTitle(int type, int ix) {
		String title = "";
		if (type == Sensor.TYPE_ACCELEROMETER) {
			if (ix == 1) {
				title = "X ";
			}
			else if (ix == 2) {
				title = "Y ";
			}
			else if (ix == 3) {
				title = "Z ";
			}
			else if (ix == 4) {
				title = "G ";
			}
		}
		else if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_GRAVITY) {
			if (ix == 1) {
				title = "X ";
			}
			else if (ix == 2) {
				title = "Y ";
			}
			else if (ix == 3) {
				title = "Z ";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_GYROSCOPE) {
			if (ix == 1) {
				title = "X ";
			}
			else if (ix == 2) {
				title = "Y ";
			}
			else if (ix == 3) {
				title = "Z ";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_LIGHT) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_LINEAR_ACCELERATION) {
			if (ix == 1) {
				title = "X ";
			}
			else if (ix == 2) {
				title = "Y ";
			}
			else if (ix == 3) {
				title = "Z ";
			}
			else if (ix == 4) {
				title = "G ";
			}
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			if (ix == 1) {
				title = "X ";
			}
			else if (ix == 2) {
				title = "Y ";
			}
			else if (ix == 3) {
				title = "Z ";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_ORIENTATION) {
			String azimuthText = getResources().getString(R.string.orientation_azimuth);
			String pitchText = getResources().getString(R.string.orientation_pitch);
			String rollText = getResources().getString(R.string.orientation_roll);
			
			if (ix == 1) {
				title = azimuthText + " ";
			}
			else if (ix == 2) {
				title = pitchText + " ";
			}
			else if (ix == 3) {
				title = rollText + " ";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_PROXIMITY) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_RELATIVE_HUMIDITY) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_ROTATION_VECTOR) {
			if (ix == 1) {
				title = "x*sin(θ/2) ";
			}
			else if (ix == 2) {
				title = "y*sin(θ/2)  ";
			}
			else if (ix == 3) {
				title = "z*sin(θ/2) ";
			}
			else if (ix == 4) {
				title = "cos(θ/2) ";
			}
			else if (ix == 5) {
				title = "A. ";
			}
		}
		else if (type == Sensor.TYPE_TEMPERATURE) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			if (ix == 1) {
				title = "X ";
			}
			else if (ix == 2) {
				title = "Y ";
			}
			else if (ix == 3) {
				title = "Z ";
			}
			else if (ix == 4) {
				title = "";
			}
			else if (ix == 5) {
				title = "biasX";
			}
			else if (ix == 6) {
				title = "biasY";
			}
			else if (ix == 7) {
				title = "biasZ";
			}
		}
		else if (type == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			if (ix == 1) {
				title = "x*sin(θ/2) ";
			}
			else if (ix == 2) {
				title = "y*sin(θ/2)  ";
			}
			else if (ix == 3) {
				title = "z*sin(θ/2) ";
			}
			else if (ix == 4) {
				title = "cos(θ/2) ";
			}
			else if (ix == 5) {
				title = "Est.h.Accrcy. ";
			}
		}
		else if (type == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			if (ix == 1) {
				title = "x*sin(θ/2) ";
			}
			else if (ix == 2) {
				title = "y*sin(θ/2)  ";
			}
			else if (ix == 3) {
				title = "z*sin(θ/2) ";
			}
			else if (ix == 4) {
				title = "cos(θ/2) ";
			}
			else if (ix == 5) {
				title = "A ";
			}
		}
		else if (type == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			if (ix == 1) {
				title = "X ";
			}
			else if (ix == 2) {
				title = "Y ";
			}
			else if (ix == 3) {
				title = "Z ";
			}
			else if (ix == 4) {
				title = "";
			}
			else if (ix == 5) {
				title = "x";
			}
			else if (ix == 6) {
				title = "y";
			}
			else if (ix == 7) {
				title = "z";
			}
			
		}
		else if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
			title = " ";
		}
		else if (type == Sensor.TYPE_STEP_COUNTER) {
			if (ix == 1) {
				title = "# ";
			}	
		}
		else if (type == Sensor.TYPE_STEP_DETECTOR) {
			if (ix == 1) {
				title = "Step ";
			}	
			else if (ix == 2) {
				title = "Time ";
			}
		}
		else if (type == Sensor.TYPE_HEART_RATE) {
			if (ix == 1) {
				title = "HR ";
			}	
			else if (ix == 2) {
				title = " ";
			}
		}
		else if (type == Sensor.TYPE_HEART_BEAT) {
			if (ix == 1) {
				title = "Beat ";
			}	
			else if (ix == 2) {
				title = " ";
			}
		}
		else if (type == Sensor.TYPE_MOTION_DETECT) {
			if (ix == 1) {
				title = "Motion ";
			}	
			else if (ix == 2) {
				title = " ";
			}
		}
		else if (type == Sensor.TYPE_STATIONARY_DETECT) {
			if (ix == 1) {
				title = "Stat ";
			}	
			else if (ix == 2) {
				title = " ";
			}
		}
		else if (type == Sensor.TYPE_POSE_6DOF) {
			if (ix == 1) {
				title = "X ";
			}	
			else if (ix == 2) {
				title = "Y ";
			}
			else if (ix == 3) {
				title = "Z ";
			}
			else if (ix == 4) {
				title = " ";
			}
			else if (ix == 5) {
				title = "TA X ";
			}
			else if (ix == 6) {
				title = "TA Y ";
			}
			else if (ix == 7) {
				title = "TA Z ";
			}
			else if (ix == 8) {
				title = "DQR X ";
			}
			else if (ix == 9) {
				title = "DQR Y ";
			}
			else if (ix == 10) {
				title = "DQR Z ";
			}
			else if (ix == 11) {
				title = "DTA X ";
			}
			else if (ix == 12) {
				title = "DTA Y ";
			}
			else if (ix == 13) {
				title = "DTA Z ";
			}
			else if (ix == 14) {
				title = "SeqNo ";
			}
		}		
		else if (type == SensorUtils.TYPE_TILT_DETECTOR) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == SensorUtils.TYPE_SCREEN_ORIENTATION_V3) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == SensorUtils.TYPE_RGB_IR_SENSOR_V1) {
			if (ix == 1) {
				title = " ";
			}
			else if (ix == 2) {
				title = " ";
			}
			else if (ix == 3) {
				title = " ";
			}
			else if (ix == 4) {
				title = " ";
			}
		}
		else if (type == SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == SensorUtils.TYPE_GRIP_SENSOR_V512) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == SensorUtils.TYPE_HRMLED_IR_SENSOR_V1) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == SensorUtils.TYPE_HRMLED_RED_SENSOR_V1) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		else if (type == SensorUtils.TYPE_PICK_UP_GESTURE) {
			if (ix == 1) {
				title = "";
			}
			else if (ix == 2) {
				title = "";
			}
			else if (ix == 3) {
				title = "";
			}
			else if (ix == 4) {
				title = "";
			}
		}
		return title;
	}
	
	private String setupUnit(int type, int row) {
		String unit = "";
		if (type == Sensor.TYPE_ACCELEROMETER) {
			if (row < 4) {
				unit = "m/s²";
			}	
			else {
				unit = "g";
			}
		}
		else if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			unit = "°C";
		}
		else if (type == Sensor.TYPE_GRAVITY) {
			unit = "m/s²";
		}
		else if (type == Sensor.TYPE_GYROSCOPE) {
			unit = "rad/s";
		}
		else if (type == Sensor.TYPE_LIGHT) {
			unit = "lx";
		}
		else if (type == Sensor.TYPE_LINEAR_ACCELERATION) {
			if (row < 4) {
				unit = "m/s²";
			}	
			else {
				unit = "g";
			}
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			unit = "μT";
		}
		else if (type == Sensor.TYPE_ORIENTATION) {
			unit = "deg";
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			if (row == 1) {
				unit = "hPa";
			}
			else if (row == 2) {
				unit = "m";
			}
		}
		else if (type == Sensor.TYPE_PROXIMITY) {
			unit = "cm";
		}
		else if (type == Sensor.TYPE_RELATIVE_HUMIDITY) {
			unit = "%";
		}
		else if (type == Sensor.TYPE_ROTATION_VECTOR) {
			unit = "deg";
		}
		else if (type == Sensor.TYPE_TEMPERATURE) {
			unit = "°C";
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			unit = "μT";
		}
		else if (type == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			unit = "";
		}
		else if (type == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			unit = "deg";
		}
		else if (type == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			unit = "rad/s";
		}
		else if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
			unit = "";
		}
		else if (type == Sensor.TYPE_STEP_COUNTER) {
			if (row == 1) {
				unit = " of step(s) ";
			}	
		}
		else if (type == Sensor.TYPE_STEP_DETECTOR) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
		}
		else if (type == Sensor.TYPE_HEART_RATE) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
		}
		else if (type == Sensor.TYPE_HEART_BEAT) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
		}
		else if (type == Sensor.TYPE_MOTION_DETECT) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
		}
		else if (type == Sensor.TYPE_STATIONARY_DETECT) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
		}
		else if (type == Sensor.TYPE_POSE_6DOF) {
			if (row == 1) {
				unit = "x*sin(θ/2)";
			}	
			else if (row == 2) {
				unit = "y*sin(θ/2)";
			}	
			else if (row == 3) {
				unit = "z*sin(θ/2)";
			}	
			else if (row == 4) {
				unit = "cos(θ/2)";
			}	
			else if (row == 5) {
				unit = "axis";
			}	
			else if (row == 6) {
				unit = "axis";
			}	
			else if (row == 7) {
				unit = "axis";
			}	
			else if (row == 8) {
				unit = "x*sin(θ/2)";
			}	
			else if (row == 9) {
				unit = "y*sin(θ/2)";
			}	
			else if (row == 10) {
				unit = "z*sin(θ/2)";
			}	
			else if (row == 11) {
				unit = "axis";
			}	
			else if (row == 12) {
				unit = "axis";
			}	
			else if (row == 13) {
				unit = "axis";
			}	
			else if (row == 14) {
				unit = ".";
			}	
		}		
		else if (type == SensorUtils.TYPE_TILT_DETECTOR) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
			else if (row == 3) {
				unit = " ";
			}	
			else if (row == 4) {
				unit = " ";
			}	
		}
		else if (type == SensorUtils.TYPE_SCREEN_ORIENTATION_V3) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
			else if (row == 3) {
				unit = " ";
			}	
			else if (row == 4) {
				unit = " ";
			}	
		}
		else if (type == SensorUtils.TYPE_GRIP_SENSOR_V512) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
			else if (row == 3) {
				unit = " ";
			}	
			else if (row == 4) {
				unit = " ";
			}	
		}
		else if (type == SensorUtils.TYPE_RGB_IR_SENSOR_V1) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
			else if (row == 3) {
				unit = " ";
			}	
			else if (row == 4) {
				unit = " ";
			}	
		}
		else if (type == SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
			else if (row == 3) {
				unit = " ";
			}	
			else if (row == 4) {
				unit = " ";
			}	
		}
		else if (type == SensorUtils.TYPE_HRMLED_IR_SENSOR_V1) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
			else if (row == 3) {
				unit = " ";
			}	
			else if (row == 4) {
				unit = " ";
			}	
		}
		else if (type == SensorUtils.TYPE_HRMLED_RED_SENSOR_V1) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
			else if (row == 3) {
				unit = " ";
			}	
			else if (row == 4) {
				unit = " ";
			}	
		}
		else if (type == SensorUtils.TYPE_PICK_UP_GESTURE) {
			if (row == 1) {
				unit = " ";
			}	
			else if (row == 2) {
				unit = " ";
			}	
			else if (row == 3) {
				unit = " ";
			}	
			else if (row == 4) {
				unit = " ";
			}	
		}
		return unit;
	}
	
	@SuppressLint("NewApi") 
	private boolean registerSensors() {
		boolean isSuccess = false;
		
		this.stepOffset = 0.0F;
		this.stepDetector = 0;
		this.sensor = null;
		if (selectedType == Sensor.TYPE_ACCELEROMETER) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}	
		else if (selectedType == Sensor.TYPE_GRAVITY) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_GYROSCOPE) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_LIGHT) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_LINEAR_ACCELERATION) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_MAGNETIC_FIELD) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_ORIENTATION) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_PRESSURE) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_PROXIMITY) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_RELATIVE_HUMIDITY) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_ROTATION_VECTOR) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_TEMPERATURE) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_SIGNIFICANT_MOTION) {
	        if (android.os.Build.VERSION.SDK_INT >= 18) {
	        	if (this.triggerListener != null) {
	        		this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
	        		if (this.sensor != null) {
        				isSuccess = sensorManager.requestTriggerSensor(triggerListener, sensor);
	        		}	
	        	}
	        }
		}
		else if (selectedType == Sensor.TYPE_STEP_COUNTER) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_STEP_DETECTOR) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_HEART_RATE) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == SensorUtils.TYPE_TILT_DETECTOR) {
			this.sensor = sensorManager.getDefaultSensor(SensorUtils.TYPE_TILT_DETECTOR);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == SensorUtils.TYPE_PICK_UP_GESTURE) {
			this.sensor = sensorManager.getDefaultSensor(SensorUtils.TYPE_PICK_UP_GESTURE);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_STATIONARY_DETECT) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STATIONARY_DETECT);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_MOTION_DETECT) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MOTION_DETECT);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_HEART_BEAT) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_BEAT);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == Sensor.TYPE_POSE_6DOF) {
			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_POSE_6DOF);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == SensorUtils.TYPE_SCREEN_ORIENTATION_V3) {
			this.sensor = sensorManager.getDefaultSensor(SensorUtils.TYPE_SCREEN_ORIENTATION_V3);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == SensorUtils.TYPE_GRIP_SENSOR_V512) {
			this.sensor = sensorManager.getDefaultSensor(SensorUtils.TYPE_GRIP_SENSOR_V512);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == SensorUtils.TYPE_HRMLED_IR_SENSOR_V1) {
			this.sensor = sensorManager.getDefaultSensor(SensorUtils.TYPE_HRMLED_IR_SENSOR_V1);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == SensorUtils.TYPE_HRMLED_RED_SENSOR_V1) {
			this.sensor = sensorManager.getDefaultSensor(SensorUtils.TYPE_HRMLED_RED_SENSOR_V1);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == SensorUtils.TYPE_RGB_IR_SENSOR_V1) {
			this.sensor = sensorManager.getDefaultSensor(SensorUtils.TYPE_RGB_IR_SENSOR_V1);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else if (selectedType == SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1) {
			this.sensor = sensorManager.getDefaultSensor(SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
		else {
			this.sensor = sensorManager.getDefaultSensor(selectedType);
			// 01/02/2017 
			if (sensorValueCapturing != null)
				isSuccess = sensorManager.registerListener(sensorValueCapturing, this.sensor, SensorManager.SENSOR_DELAY_UI);
			else
				isSuccess = sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_UI);
		}
//		if (isSuccess) {
			showSensorDescriptor();
//		}
		return isSuccess;	
	}
	
	private String setupAccuracyValue(int type, int value) {
		String accuracy = "";
		String intValue = "("+String.format("%+5d",value)+")";
		if (SensorManager.SENSOR_STATUS_ACCURACY_HIGH == value) {
			String highLevelText = getResources().getString(R.string.sensor_accuracy_level_high);
			accuracy = highLevelText + " " + intValue;
		}
		else if (SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM == value ) {
			String mediumLevelText = getResources().getString(R.string.sensor_accuracy_level_medium);
			accuracy = mediumLevelText + " " + intValue;
		}
		else if (SensorManager.SENSOR_STATUS_ACCURACY_LOW == value ) {
			String lowLevelText = getResources().getString(R.string.sensor_accuracy_level_low);
			accuracy = lowLevelText + " " + intValue;
		}
		else if (SensorManager.SENSOR_STATUS_UNRELIABLE == value ) {
			String unreliableLevelText = getResources().getString(R.string.sensor_accuracy_level_unreliable);
			accuracy = unreliableLevelText + " " + intValue;
		}
		return accuracy;
	}
	
	private void showAccuracyValues(int type, int value) {
		if (sensorAccuracyTitle == null) {
			sensorAccuracyTitle = (TextView) findViewById(R.id.sensorAccuracyTitle);
		}	
		
		String title = getResources().getString(R.string.sensorAccuracyTitle);
		sensorAccuracyTitle.setText(title + " " +  setupAccuracyValue(type, value));
	}
	
	private void showValues(int type, String valueX, String valueY, String valueZ, String valueW, String biasX, String biasY, String biasZ) {
		if (sensorValueX == null)
			sensorValueX = (TextView) findViewById(R.id.sensorValueX);
		if (sensorValueY == null)
			sensorValueY = (TextView) findViewById(R.id.sensorValueY);
		if (sensorValueZ == null)
			sensorValueZ = (TextView) findViewById(R.id.sensorValueZ);
		if (sensorValueW == null)
			sensorValueW = (TextView) findViewById(R.id.sensorValueW);
		if (sensorValuebX == null)
			sensorValuebX = (TextView) findViewById(R.id.sensorValuebX);
		if (sensorValuebY == null)
			sensorValuebY = (TextView) findViewById(R.id.sensorValuebY);
		if (sensorValuebZ == null)
			sensorValuebZ = (TextView) findViewById(R.id.sensorValuebZ);

		
		String valuex = "";
		String valuey = "";
		String valuez = "";
		String valuew = "";
		String valuebx = "";
		String valueby = "";
		String valuebz = "";
		
		if (type == Sensor.TYPE_ACCELEROMETER) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			if (!valueW.equals(""))
				valuew = setupTitle(type, 4);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			if (!valueW.equals(""))
				valuew += " " + valueW;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			if (!valueW.equals(""))
				valuew += " " + setupUnit(type, 4);
		}
		else if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			valuex = setupTitle(type, -1);
			valuey = setupTitle(type, -1);
			valuez = setupTitle(type, -1);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;

			valuex += " " + setupUnit(type, 1);
//			valuey += " " + setupUnit(type, 2);
//			valuez += " " + setupUnit(type, 3);
		}
		else if (type == Sensor.TYPE_GRAVITY) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
		}
		else if (type == Sensor.TYPE_GYROSCOPE) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
		}
		else if (type == Sensor.TYPE_LIGHT) {
			valuex = setupTitle(type, -1);
			valuey = setupTitle(type, -1);
			valuez = setupTitle(type, -1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_LINEAR_ACCELERATION) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			if (!valueW.equals(""))
				valuew = setupTitle(type, 4);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			if (!valueW.equals(""))
				valuew += " " + valueW;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			if (!valueW.equals(""))
				valuew += " " + setupUnit(type, 4);
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
		}
		else if (type == Sensor.TYPE_ORIENTATION) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			valuex = setupTitle(type, -1);
			valuey = setupTitle(type, -1);
			valuez = setupTitle(type, -1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
			
			valuey += " " + valueY;

			valuey += " " + setupUnit(type, 2);
			
		}
		else if (type == Sensor.TYPE_PROXIMITY) {
			valuex = setupTitle(type, -1);
			valuey = setupTitle(type, -1);
			valuez = setupTitle(type, -1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_RELATIVE_HUMIDITY) {
			valuex = setupTitle(type, -1);
			valuey = setupTitle(type, -1);
			valuez = setupTitle(type, -1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_ROTATION_VECTOR) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			if (!valueW.equals(""))
				valuew = setupTitle(type, 4);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			if (!valueW.equals(""))
				valuew += " " + valueW;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			if (!valueW.equals(""))
				valuew += " " + setupUnit(type, 4);
		}
		else if (type == Sensor.TYPE_TEMPERATURE) {
			valuex = setupTitle(type, -1);
			valuey = setupTitle(type, -1);
			valuez = setupTitle(type, -1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);

			valuew = setupTitle(type, 4);
			
			valuebx = setupTitle(type, 5);
			valueby = setupTitle(type, 6);
			valuebz = setupTitle(type, 7);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;

			valuebx += " " + biasX;
			valueby += " " + biasY;
			valuebz += " " + biasZ;
			
			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			
			valuebx += " " + setupUnit(type, 5);
			valueby += " " + setupUnit(type, 6);
			valuebz += " " + setupUnit(type, 7);
			
		}
		else if (type == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			if (!valueW.equals(""))
				valuew = setupTitle(type, 4);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			if (!valueW.equals(""))
				valuew += " " + valueW;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			if (!valueW.equals(""))
				valuew += " " + setupUnit(type, 4);
		}
		else if (type == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			if (!valueW.equals(""))
				valuew = setupTitle(type, 4);

			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			if (!valueW.equals(""))
				valuew += " " + valueW;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			if (!valueW.equals(""))
				valuew += " " + setupUnit(type, 4);
		}
		else if (type == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);

			valuebx = setupTitle(type, 5);
			valueby = setupTitle(type, 6);
			valuebz = setupTitle(type, 7);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			valuebx += " " + biasX;
			valueby += " " + biasY;
			valuebz += " " + biasZ;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			
			valuebx += " " + setupUnit(type, 5);
			valueby += " " + setupUnit(type, 6);
			valuebz += " " + setupUnit(type, 7);
		}
		else if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
			valuex = setupTitle(type, 1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_STEP_COUNTER) {
			valuex = setupTitle(type, 1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_STEP_DETECTOR) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);

			valuex += " " + valueX;
			valuey += " " + valueY;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
		}
		else if (type == Sensor.TYPE_HEART_RATE) {
			valuex = setupTitle(type, 1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_HEART_BEAT) {
			valuex = setupTitle(type, 1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_MOTION_DETECT) {
			valuex = setupTitle(type, 1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == Sensor.TYPE_POSE_6DOF) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);

			valuebx = setupTitle(type, 5);
			valueby = setupTitle(type, 6);
			valuebz = setupTitle(type, 7);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			
			valuebx += " " + biasX;
			valueby += " " + biasY;
			valuebz += " " + biasZ;

			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			
			valuebx += " " + setupUnit(type, 5);
			valueby += " " + setupUnit(type, 6);
			valuebz += " " + setupUnit(type, 7);		}
		else if (type == Sensor.TYPE_STATIONARY_DETECT) {
			valuex = setupTitle(type, 1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == SensorUtils.TYPE_TILT_DETECTOR) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			valuebx = setupTitle(type, 5);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			valuex += " " + biasX;
			
			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			valuebx += " " + setupUnit(type, 5);
		}
		else if (type == SensorUtils.TYPE_SCREEN_ORIENTATION_V3) {
			valuex = setupTitle(type, 1);

			valuex += " " + valueX;

			valuex += " " + setupUnit(type, 1);
		}
		else if (type == SensorUtils.TYPE_GRIP_SENSOR_V512) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			valuebx = setupTitle(type, 5);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			valuex += " " + biasX;
			
			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			valuebx += " " + setupUnit(type, 5);
		}
		else if (type == SensorUtils.TYPE_RGB_IR_SENSOR_V1) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			valuebx = setupTitle(type, 5);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			valuex += " " + biasX;
			
			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			valuebx += " " + setupUnit(type, 5);
		}
		else if (type == SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			valuebx = setupTitle(type, 5);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			valuex += " " + biasX;
			
			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			valuebx += " " + setupUnit(type, 5);
		}
		else if (type == SensorUtils.TYPE_HRMLED_IR_SENSOR_V1) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			valuebx = setupTitle(type, 5);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			valuex += " " + biasX;
			
			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			valuebx += " " + setupUnit(type, 5);
		}
		else if (type == SensorUtils.TYPE_HRMLED_RED_SENSOR_V1) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			valuebx = setupTitle(type, 5);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			valuex += " " + biasX;
			
			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			valuebx += " " + setupUnit(type, 5);
		}
		else if (type == SensorUtils.TYPE_PICK_UP_GESTURE) {
			valuex = setupTitle(type, 1);
			valuey = setupTitle(type, 2);
			valuez = setupTitle(type, 3);
			valuebx = setupTitle(type, 5);
			
			valuex += " " + valueX;
			valuey += " " + valueY;
			valuez += " " + valueZ;
			valuex += " " + biasX;
			
			valuex += " " + setupUnit(type, 1);
			valuey += " " + setupUnit(type, 2);
			valuez += " " + setupUnit(type, 3);
			valuebx += " " + setupUnit(type, 5);
		}
		
		sensorValueX.setText(valuex);
		sensorValueY.setText(valuey);
		sensorValueZ.setText(valuez);
		
		sensorValueW.setText(valuew);

		sensorValuebX.setText(valuebx);
		sensorValuebY.setText(valueby);
		sensorValuebZ.setText(valuebz);
	}	
	
	private void setupSensorName(int type) {
		if (sensorTextView == null)
			sensorTextView = (TextView) findViewById(R.id.sensorNameValue);
		
		if (type == Sensor.TYPE_ACCELEROMETER) {
			sensorTextView.setText(getResources().getString(R.string.accelerometer_text_title));
		}
		else if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
			sensorTextView.setText(getResources().getString(R.string.ambienttemperature_text_title));
		}
		else if (type == Sensor.TYPE_GRAVITY) {
			sensorTextView.setText(getResources().getString(R.string.gravity_text_title));
		}
		else if (type == Sensor.TYPE_GYROSCOPE) {
			sensorTextView.setText(getResources().getString(R.string.gyroscope_text_title));
		}
		else if (type == Sensor.TYPE_LIGHT) {
			sensorTextView.setText(getResources().getString(R.string.light_text_title));
		}
		else if (type == Sensor.TYPE_LINEAR_ACCELERATION) {
			sensorTextView.setText(getResources().getString(R.string.linearacceleration_text_title));
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			sensorTextView.setText(getResources().getString(R.string.magneticfield_text_title));
		}
		else if (type == Sensor.TYPE_ORIENTATION) {
			sensorTextView.setText(getResources().getString(R.string.orientation_text_title));
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			sensorTextView.setText(getResources().getString(R.string.presure_text_title));
		}
		else if (type == Sensor.TYPE_PRESSURE) {
			sensorTextView.setText(getResources().getString(R.string.presure_text_title));
		}
		else if (type == Sensor.TYPE_PROXIMITY) {
			sensorTextView.setText(getResources().getString(R.string.proximity_text_title));
		}
		else if (type == Sensor.TYPE_RELATIVE_HUMIDITY) {
			sensorTextView.setText(getResources().getString(R.string.relativehumidity_text_title));
		}
		else if (type == Sensor.TYPE_ROTATION_VECTOR) {
			sensorTextView.setText(getResources().getString(R.string.rotationvector_text_title));
		}
		else if (type == Sensor.TYPE_TEMPERATURE) {
			sensorTextView.setText(getResources().getString(R.string.temperature_text_title));
		}
		else if (type == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
			sensorTextView.setText(getResources().getString(R.string.uncalibratedmagneticfield_text_title));
		}
		else if (type == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
			sensorTextView.setText(getResources().getString(R.string.geomagrotationvector_text_title));
		}
		else if (type == Sensor.TYPE_GAME_ROTATION_VECTOR) {
			sensorTextView.setText(getResources().getString(R.string.gamerotationvector_text_title));
		}
		else if (type == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			sensorTextView.setText(getResources().getString(R.string.uncalibrategyroscope_text_title));
		}
		else if (type == Sensor.TYPE_SIGNIFICANT_MOTION) {
			sensorTextView.setText(getResources().getString(R.string.significantmotion_text_title));
		}
		else if (type == Sensor.TYPE_STEP_COUNTER) {
			sensorTextView.setText(getResources().getString(R.string.stepcounter_text_title));
		}
		else if (type == Sensor.TYPE_STEP_DETECTOR) {
			sensorTextView.setText(getResources().getString(R.string.stepdetector_text_title));
		}
		else if (type == Sensor.TYPE_HEART_RATE) {
			sensorTextView.setText(getResources().getString(R.string.heartrate_text_title));
		}
		else if (type == Sensor.TYPE_HEART_BEAT) {
			sensorTextView.setText(getResources().getString(R.string.heartbeat_text_title));
		}
		else if (type == Sensor.TYPE_MOTION_DETECT) {
			sensorTextView.setText(getResources().getString(R.string.motiondetect_text_title));
		}
		else if (type == Sensor.TYPE_POSE_6DOF) {
			sensorTextView.setText(getResources().getString(R.string.posedof_text_title));
		}
		else if (type == Sensor.TYPE_STATIONARY_DETECT) {
			sensorTextView.setText(getResources().getString(R.string.stationarydetect_text_title));
		}
		else if (type == SensorUtils.TYPE_TILT_DETECTOR) {
			sensorTextView.setText(getResources().getString(R.string.tiltdetector_text_title));
		}
		else if (type == SensorUtils.TYPE_SCREEN_ORIENTATION_V3) {
			sensorTextView.setText(getResources().getString(R.string.screenorientation_text_title));
		}
		else if (type == SensorUtils.TYPE_GRIP_SENSOR_V512) {
			sensorTextView.setText(getResources().getString(R.string.grip_text_title));
		}
		else if (type == SensorUtils.TYPE_RGB_IR_SENSOR_V1) {
			sensorTextView.setText(getResources().getString(R.string.rgbir_text_title));
		}
		else if (type == SensorUtils.TYPE_INTERRUPT_GYROSCOPE_SENSOR_V1) {
			sensorTextView.setText(getResources().getString(R.string.interruptgyroscope_text_title));
		}
		else if (type == SensorUtils.TYPE_HRMLED_IR_SENSOR_V1) {
			sensorTextView.setText(getResources().getString(R.string.hrmled_ir_text_title));
		}
		else if (type == SensorUtils.TYPE_HRMLED_RED_SENSOR_V1) {
			sensorTextView.setText(getResources().getString(R.string.hrmled_red_text_title));
		}
		else if (type == SensorUtils.TYPE_PICK_UP_GESTURE) {
			sensorTextView.setText(getResources().getString(R.string.pickup_gesture_text_title));
		}
		// 01/02/2017
		if (internalTemperature != null) {
			String name = (String) sensorTextView.getText();
			name += " " + internalTemperature;
			sensorTextView.setText(name);
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor_values);
		
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		
		
        context = getApplicationContext();
        
		this.selectedType = (int) getIntent().getExtras().getLong(Main.BUNDLE_CONTENT_TAG_DATA1);
		setupSensorName(this.selectedType);
		
        if (android.os.Build.VERSION.SDK_INT >= 18) {
        	this.triggerListener = new SVTriggerListener();
        }	
        
        if (android.os.Build.VERSION.SDK_INT > 23) {
        	sensorValueCapturing = new SensorValueCapturing();        	
        }
		
		// Set Sensor Manager
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
	
        
	}

	/**
	 * 
	 */
	@SuppressLint("NewApi") 
	private void showSensorDescriptor() {
		if (sensorNameTextView == null) {
			sensorNameTextView = (TextView) findViewById(R.id.sensorName);
		}
		if (sensorPowerTextView == null) {
			sensorPowerTextView = (TextView) findViewById(R.id.sensorPower);
		}	
		if (sensorMinDelayTextView == null) {
			sensorMinDelayTextView = (TextView) findViewById(R.id.sensorMinDelay);
		}
		if (sensorResolutionTextView == null) {
			sensorResolutionTextView = (TextView) findViewById(R.id.sensorResolution);
		}	
		if (sensorMaxRangeTextView == null) {
			sensorMaxRangeTextView = (TextView) findViewById(R.id.sensorMaxRange);
		}	
		
		if (sensor == null) {
			sensorNameTextView.setText("-");
			sensorPowerTextView.setText(getResources().getString(R.string.sensor_details_power) + " -");
			sensorMinDelayTextView.setText(getResources().getString(R.string.sensor_details_mindelay) + " -");
			sensorResolutionTextView.setText(getResources().getString(R.string.sensor_details_resolution) + " -");
			sensorMaxRangeTextView.setText(getResources().getString(R.string.sensor_details_maxrange) + " -");
			return;
		}	
		
		String text = sensor.getName() + " v" + sensor.getVersion() + " by " + sensor.getVendor();
		sensorNameTextView.setText(text);

		text = "";
		text = getResources().getString(R.string.sensor_details_power) + " " + sensor.getPower() + "mA";
   		sensorPowerTextView.setText(text);
			
		text = "";
		 String title = getResources().getString(R.string.sensor_details_mindelay) + " "; 
		 if (android.os.Build.VERSION.SDK_INT >= 9) {
			 text = title + sensor.getMinDelay();
			 sensorMinDelayTextView.setText(text);
		 }
		 else {
			 text = title + " -";
			 sensorMinDelayTextView.setText(text);
		 }
		 
		text = "";
		text = getResources().getString(R.string.sensor_details_resolution) + " " + sensor.getResolution();
		sensorResolutionTextView.setText(text);

		text = "";
		text = getResources().getString(R.string.sensor_details_maxrange) + " " + sensor.getMaximumRange();
		sensorMaxRangeTextView.setText(text);
	}
	
	@Override
	protected void onResume() {
    	registerSensors();
		super.onResume();
	}
	
	@SuppressLint("NewApi") 
	@Override
	protected void onPause() {
		if (selectedType == Sensor.TYPE_SIGNIFICANT_MOTION) {
	        if (android.os.Build.VERSION.SDK_INT >= 18) {
	        	if (this.triggerListener != null) {
	        		this.sensorManager.cancelTriggerSensor(this.triggerListener, this.sensor);
	        	}
	        }
		}
		else {
			// 01/02/2017 
			if (sensorValueCapturing != null)
				this.sensorManager.unregisterListener(sensorValueCapturing);
			else
				this.sensorManager.unregisterListener(this);
		}	
    	this.sensor = null;
		super.onPause();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int value) {
		if (sensor.getType() != selectedType) {
			return;
		}
		showAccuracyValues(sensor.getType(), value);
	}

	/**
	 * 
	 * @param accelerationValues
	 * @param magneticValues
	 * @return
	 */
	private float[] generateRotationMatrix(float[] accelerationValues, float[] magneticValues) {
		float[] rotationMatrix = null;
		if (accelerationValues != null && magneticValues != null) {
			rotationMatrix = new float[16];
			boolean isRotationMatrixGenerated;
			isRotationMatrixGenerated = SensorManager.getRotationMatrix(rotationMatrix, null, accelerationValues, magneticValues);
			if (!isRotationMatrixGenerated) {
				rotationMatrix = null;
			}
		}
		return rotationMatrix;
	}
	
	/**
	 * 
	 * @param rotationMatrix
	 * @return
	 */
	private float[] determineOrientation(float[] rotationMatrix) {
		float[] orientationValues = new float[3];
		SensorManager.getOrientation(rotationMatrix, orientationValues);
		// azimuth
		orientationValues[0] = (float) Math.toDegrees(orientationValues[0]);
		// pitch
		orientationValues[1] = (float) Math.toDegrees(orientationValues[1]);
		// roll
		orientationValues[2] = (float) Math.toDegrees(orientationValues[2]);
		return orientationValues; 
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		handlingSensorChanged(event);
	}
}
