package com.lkovari.mobile.apps.sensors;

import java.text.DecimalFormatSymbols;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * 
 * @author lkovari
 *
 */
public class SensorSettings {
	
	public static SensorsShowOptionKind SENSOR_SHOW_OPTION_KIND = SensorsShowOptionKind.ALL_SENSORS;
	//# 2015.08.15. set 1013.25 hPa (milibar)
	public static double SENSOR_PRESURE_FOR_ALTIMETER_QNH = SensorManager.PRESSURE_STANDARD_ATMOSPHERE;

	
	/**
	 * 
	 * @param context Context - 
	 * @param str String - the typed QNV value in string
	 * @return boolean - return true if it is a numeric value, return false if it is contains non numeric character
	 */
	public static boolean isNumeric(Context context, String str) {
		Locale currentLocale = context.getResources().getConfiguration().locale;
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(currentLocale);
	    for (char c : str.toCharArray()) {
	        if (!Character.isDigit(c) && (c != dfs.getDecimalSeparator())) 
	        	return false;
	    }
	    return true;
	}	
	
	/**
	 * 2015.08.15.
	 * @param context
	 */
	public static void loadPreferences(Context context) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String optionkindValue = sharedPreferences.getString("key_sensorshowoptionkind", "0");
		if (optionkindValue.equals("0")) {
			SensorSettings.SENSOR_SHOW_OPTION_KIND = SensorsShowOptionKind.ALL_SENSORS;
		}
		else if (optionkindValue.equals("1")) {
			SensorSettings.SENSOR_SHOW_OPTION_KIND = SensorsShowOptionKind.AVAILABLE_SENSORS;
		}
		String defaultValue = "" + SensorSettings.SENSOR_PRESURE_FOR_ALTIMETER_QNH;
		String qnhValue = sharedPreferences.getString("key_qnh_value", defaultValue);
		double qnh_value = SensorSettings.SENSOR_PRESURE_FOR_ALTIMETER_QNH;
		try {
			if (isNumeric(context, qnhValue)) {
				qnh_value = Double.parseDouble(qnhValue);
			}
			else {
				Toast.makeText(context, R.string.error_qnh_value_invalid, Toast.LENGTH_LONG).show();
			}
		}
		catch (Exception e) {
			Toast.makeText(context, R.string.error_qnh_value_invalid, Toast.LENGTH_LONG).show();
		}
		SensorSettings.SENSOR_PRESURE_FOR_ALTIMETER_QNH = qnh_value;
	}	
	
}
